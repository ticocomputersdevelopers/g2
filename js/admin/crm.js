 var base_url = $('#base_url').val();

 function translate(neki_string) {
         return neki_string;
     }

 $(document).ready(function() {
     /* Tamara html dodato */
     HTMLElement.prototype.click = function() {
         var evt = this.ownerDocument.createEvent('MouseEvents');
         evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
         this.dispatchEvent(evt);
     }
     var base_url = $('#base_url').val();

     // $('#admin-login').click(function() {

     //     var username = $('#username').val();
     //     var password = $('#password').val();
     //     if (username == '' || username == ' ') {
     //         $('#username').css("border", "1px solid red").focus().fadeIn(200);
     //     } else if (password == '' || password == ' ') {
     //         $('#password').css("border", "1px solid red").focus().fadeIn(200);
     //     } else {

     //         $.ajax({
     //             type: "POST",
     //             url: base_url + 'admin_login',
     //             data: { username: username, password: password },
     //             success: function(msg) {

     //                 switch (msg) {
     //                     case "1":
     //                         // popupOpen();
     //                         // setTimeout(function() {
     //                         //     popupClose();
     //                         // }, 2000);
     //                         // $('.info-popup .popup-inner').html("<div class='popup-close'><i class='fa fa-times'></i></div> <p class='p-info'>Korisnik ili lozinka su pogrešni.</p>");
     //                         // break;
     //                        alert('Korisnik ili lozinka su pogrešni!');
     //                     case "2":
     //                         // popupOpen();
     //                         // setTimeout(function() {
     //                         //     popupClose();
     //                         // }, 2000);
     //                         // $('.info-popup .popup-inner').html(" <div class='popup-close'></div> <p class='p-info'>Vaš nalog je suspendovan.</p>");
     //                         // break;
     //                         alert('Vaš nalog je suspendovan!');
     //                     case "0":
     //                         window.location.href = base_url + 'admin';
     //                         break;
     //                 }
     //             }
     //         });
     //     }

     //     // return false;
     // });
      function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;

    }

     function popupOpen() {
         $('.info-popup').addClass('popup-opened');
     }

     function popupClose() {
         $('.info-popup').removeClass('popup-opened');
     }
    
    $(".search_select").select2({
        width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    });

     $('.page-edit-save').click(function() {
         var naslov = $('#page_name').val();
         if (naslov == '' || naslov == ' ') {
             $('#page_name').css("border", "1px solid red").focus().fadeIn(200);
         } else {

             form_pages.submit();
         }

     });

     $('.JSosobine').on('change', function(){
        var osobina_naziv_id = $(this).find(':selected').data('id');
        var roba_id = $(this).find(':selected').data('roba');
        if( osobina_naziv_id !=null )
        location.href = base_url + 'admin/product_osobine/'+ roba_id + '/' + osobina_naziv_id;
        else{
        location.href = base_url + 'admin/product_osobine/'+ roba_id + '/0' ;
        }
    });

     $('.logo-upload-confirm').click(function() {
         logo_upload.submit();
     });

     $('.social').change(function() {
         var vrednost = $(this).val();
         var polje = $(this).data('naziv');
         // alert(vrednost+" "+polje);
         $.ajax({
             type: "POST",
             url: base_url + 'admin/social_update',
             data: { vrednost: vrednost, polje: polje }
         });

     });

     $('.banner-edit-save').click(function() {
         var naziv = $("#naziv").val();
         var link = $("#link").val();
         if (naziv == '' || naziv == ' ') {
             $('#naziv').css("border", "1px solid red").focus().fadeIn(200);
         } else if (link == '' || link == ' ') {
             $('#link').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             banner_upload.submit();
         }


     });

     $('.JSWebOptions').click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        $(this).addClass('active');
        $(this).siblings('.JSWebOptions').removeClass('active');

        if(id==203){
            if(id==203 && $(this).attr('id') == 'JSPrikazDa'){
                $('.JSOptionPrikazSifra').removeAttr('disabled');
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/setings_update',
                    data: { id: id, status: status },
                });
            }else if(id==203 && $(this).attr('id') == 'JSPrikazNe'){
                $('.JSOptionPrikazSifra').attr('disabled', 'disabled');
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/setings_update',
                    data: { id: id, status: status },
                });
                
            }
        }else{
            $.ajax({
                type: "POST",
                url: base_url + 'admin/setings_update',
                data: { id: id, status: status },
            });
        }
         });
    
     $('.JSOptions').click(function() {
         $(this).addClass('active');
         $(this).siblings('.JSOptions').removeClass('active');
         var id = $(this).data('id');
         var status = $(this).data('status');
         // alert(base_url+'admin/setings_update');
         $.ajax({
             type: "POST",
             url: base_url + 'admin/options_setings_update',
             data: { id: id, status: status },

         });
     });

   $('.JSOptionsChangeStrData').on('change',function () {
        var value = $(this).val();
        var id = $(this).data('id');

        $.post(base_url+'admin/options-change-str-data', {id: id, value: value}, function (response){});

   });


    //pregled i edit akcija
    $('.JSActionEdit').click(function(e) {
            var akcija_id = $(this).data('id-akcija');

            if (e.target.tagName.toLowerCase() != 'select' && e.target.tagName.toLowerCase() != 'i') { 
                window.location.href = base_url + 'crm/crm_home/crm_akcija/' + akcija_id;  
            }
           
    });

// SELECTED ROW
$('.JScheck_action').on('click', function(e){  
    var id = $(this).data('e-selected');
        // next_table = $(this).closest('tr').next('tr').find('table.hide');

    sessionStorage.setItem('id', id);   
    // if (next_table.is('.hide')) {
    //     e.preventDefault();
    //     next_table.toggle(); 
    //     sessionStorage.removeItem('id');   
    // }  
});
    
$('.JScheck_action').each(function(i, el){
    if ($(el).data('e-selected') == sessionStorage.getItem('id')) {
        // $(el).css('color', '#00e600');

        // ALL ACTIONS SHOW
        $(el).closest('tr').css('background','#d2dfd9').after('<tr><td class="JSactions_content_row no-padd" colspan="100%"></td></tr>');
        // $('.JSactions_content_row').append($('.JSactions_content_table'));
        // $('.JSactions_content_row').children().show();
    }
});
// $('.JSStatusIdValue').each(function(el){
//      var id = $(this).data('e-selected'),
//      sessionStorage.setItem('id', id); 
// });


//ADD BACKGROUND ON CRM
if ($('.crm_page').length  && !(sessionStorage.getItem('crm_page'))) {
    var crm_bg = $('.absolute_bg');

    crm_bg.removeClass('hide');
     $(document).on('click', function(){
        crm_bg.remove();
    });
    if (crm_bg.length) {
        sessionStorage.setItem('crm_page','1');
    }
} 

// //show crm action on click
$('.crm_li div').each(function(){
    $(this).on('click', function(){

        $(this).parent('li').toggleClass('active_li_crm');
        $(this).parent('li').siblings('li').removeClass('active_li_crm');

    });
});

$('.JSTodayAction').click(function(){
    // $(this).closest('.JSUlTodayActions').find('ul').addClass('hide');

    var element = $(this).parent().find('ul');
    if($(this).parent().find('ul').hasClass('hide')){
    //console.log(element);
        element.removeClass('hide');
    }
    // else{
    //     element.addClass('hide');
    // }
});

//modal partner id partner
$(document).on("click", ".open-add-contact", function () {
     var myPartnerId = $(this).data('id'); 
      $.ajax({
             type: "POST",
             url: base_url + 'crm/partner_details',
             data: { myPartnerId: myPartnerId },
             success: function(response) {
                 // // alert(msg);
               
                 // /console.log( response);
                 $('#JSPartnerDetailsModalContent').html(response);
                 $('#add-contact').foundation('reveal', 'open');

             }
         });
   
});

//modal today actions
$(document).on("click", ".open-today-actions", function () {
    $('#crmTodayActions').foundation('reveal', 'open');
});

// Open partner modal if field is not fill
if($('#add-partner').find('.error').text()) {
    $('#add-partner').foundation('reveal', 'open');
}

//modal dokumenta id crma
$(document).on("click", ".open-documentNew", function () {
     var crmID = $(this).data('id');
     
      $.ajax({
             type: "POST",
             url: base_url + 'crm/partner_document',
             data: { crmID: crmID },
             success: function(response) {
                // console.log( response);
                 $('#JSDocumentNew').html(response);
                 $('#documentNew').foundation('reveal', 'open');

             }
         });
   
});

//modal partner info sve akcije
$(document).on("click", ".open-partnerDetailsModal", function () {
     var CrmId = $(this).data('id');

      $.ajax({
             type: "POST",
             url: base_url + 'crm/crm_ispis',
             data: { CrmId: CrmId },
             success: function(response) {
                 // // alert(msg);
                 //console.log( response);
                 $('#JSPartnerAllActions').html(response);
                 $('#partnerDetailsModal').foundation('reveal', 'open');

             }
         });
   
});

//modal dokumenta fakture
$(document).on("click", ".open-accountNew", function () {
     var partnerID = $(this).data('id');
     
      $.ajax({
             type: "POST",
             url: base_url + 'crm/crm_fakture_dokumenta',
             data: { partnerID: partnerID },
             success: function(response) {
                 //alert(partnerID);
                 $('#JSAccountNew').html(response);
                 $('#accountNew').foundation('reveal', 'open');

             }
         });
   
});


//modal edit akcije

$(document).on("click", ".JSedit_action", function () {
     var crmAkcijaId = $(this).data('id');
     //console.log(crmAkcijaId);
     $("#akcijaID").val(crmAkcijaId);
     document.getElementById("akcijaID").value = crmAkcijaId;
});

//vreme i datum akcije
$(document).ready(function(){
    $('.JSdatepickerAction').datepicker();
  });



$(document).on('click', function(e){

    if ($('.JScustom_select, .JScustom_select *').is(e.target)) {


        $(e.target).closest('td').find('.checkboxes').toggle().closest('tr').siblings('tr').find('.checkboxes').hide();   
        

    } else {

        if ($('.JScrm_manager, .JScrm_manager *').is(e.target)) {
            
            return;

        } else {

            $('.checkboxes').hide();  
        }
       
    }  
 
}); 


    $('input[name="datum_pocetka"]').datepicker({
        format: 'Y-m-d'
    });

    $('input[name="datum_kraja"]').datepicker({
        format: 'Y-m-d'
    });

// $('select[name="crm_status_id"]').select2({
//         placeholder: translate('Izaberi status')
//     });
    //$('select[name="crm_tip_id"]').select2({placeholder: 'Izaberi tip'});
    

//search
$('input[name="datum_pocetka"]').change(function(){
        var params = getUrlVars();
        delete params['page'];
        params['datum_pocetka'] = $(this).val();

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    }); 

    $('input[name="datum_kraja"]').change(function(){
        var params = getUrlVars();
        delete params['page'];
        
        params['datum_kraja'] = $(this).val();

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    }); 
//pretraga crm
$('#JSCRMSearch').click(function(){
        var search = $('input[name="search"]').val() == '' ? 'null' : $('input[name="search"]').val();
        var params = getUrlVars();
        delete params['page'];
        params['search'] = search;

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    });
//pretragaa fakture
$('#JSCRMfakture').click(function(){
        var search = $('input[name="search"]').val() == '' ? 'null' : $('input[name="search"]').val();
        var params = getUrlVars();
        delete params['page'];
        params['search'] = search;

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    });


//filter aktivam

$('.JSactiveCrm').click(function() {
        var aktivni = $('input[name="aktivni"]').val() == '' ? 'null' : $('input[name="aktivni"]').val();
        var params = getUrlVars();
        delete params['page'];
        params['aktivni'] = aktivni;

    window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
});
//Filter logova
    $('#JSLogoviSearch').click(function(){
        var params = getUrlVars();
        var val = $('select[name="crm_logovi_akcija"]').val();
        if(val){
            params['crm_logovi_akcija'] = $('select[name="crm_logovi_akcija"]').val();
        }else{
            delete params['crm_logovi_akcija'];
        }
        delete params['page'];

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    });
  $('#JSKorisnikSearch').click(function(){
        var params = getUrlVars();
        var val = $('select[name="imenik_id"]').val();
        if(val){
            params['imenik_id'] = $('select[name="imenik_id"]').val();
        }else{
            delete params['imenik_id'];
        }
        delete params['page'];

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    });




//filteri statusa 
    $('#JSStatusiSearch').click(function(){
        var params = getUrlVars();
        var val = $('select[name="crm_status_id"]').val();
        if(val){
            params['crm_status_id'] = $('select[name="crm_status_id"]').val();//ovde bi trebalo da se dodaju vrednosti .join('-')
        }else{
            delete params['crm_status_id'];
        }
        delete params['page'];
        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    }); 


//filteri tipova JSStatusiClear
    $('#JSTipoviSearch').click(function(){
        var params = getUrlVars();
        var val = $('select[name="crm_tip_id"]').val();
        if(val){
            params['crm_tip_id'] = $('select[name="crm_tip_id"]').val();
        }else{
            delete params['crm_tip_id'];
        }
        delete params['page'];

        window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
    });


//pretraga  robe crm 
var articlesTime = 0;
    $('#JSRobaSearch').on("keyup", function() {
                clearTimeout(articlesTime);
                articlesTime = setTimeout(function(){
                    $('.articles_list').remove();
                    var articles = $('#JSRobaSearch').val();
                    
                    if (articles != '' && articles.length > 2) {
                        $.post(base_url + 'crm/ajax/search_articles', {articles:articles}, function (response){
                            $('#content_artikli').html(response);
                        });
                    }
                }, 500);
    });

//analitika crm 
 $(function() {
        $( "#datum_do_crm, #datum_od_crm" ).datepicker();
    });

    $('#datum_do_crm').keydown(false);
    $('#datum_od_crm').keydown(false);

    //
    $('#datum_od_crm').change(function() {
     var datum_od_crm = $(this).val();
     var datum_do_crm = $('#datum_do_crm').val();

    if(datum_do_crm != '' && datum_od_crm != ''){
        window.location.href = base_url + 'crm/crm_analitika/' + datum_od_crm + '/' + datum_do_crm;
    }

    });
    $('#datum_do_crm').change(function() {
     var datum_do_crm = $(this).val();
     var datum_od_crm = $('#datum_od_crm').val();

    if(datum_do_crm != '' && datum_od_crm != ''){
        window.location.href = base_url + 'crm/crm_analitika/' + datum_od_crm + '/' + datum_do_crm;
    }
    });

    //export faktura 
    $('.JSExportFaktura').on('click', function(){
    $('#JSselectField').foundation('reveal', 'open');
    });
    $('.JSExportFak').click(function(){
        var names = $('.JSChooseExport:checked').map(function(idx, elem) {
            return $(elem).data('name');
          }).get();

        var url_names = 'null';
        if(names.length > 0){
            url_names = names.join('-');
        }

        var params = getUrlVars();
        params['export'] = 1;
        params['vrsta_fajla'] = $(this).data('kind');
        params['imena_kolona'] = url_names;

        window.location.href = window.location.pathname+'?'+$.param(params);
    });
     $(".JSManagerSelect").select2({
        placeholder: ('Izaberi komercijalistu')
    });


    var table =  $('.JSdate_time').closest('table');
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
    this.dsc = !this.dsc;
    if (!this.asc){ rows = rows.reverse(); }
    for (var i = 0; i < rows.length; i++){ table.append(rows[i]) }  


 });


 // CRM





//sortiranje po datumu 

// START
// document.getElementById("JSdefaultSort").onload = function() {defaultSort()
//     var table = $(this).closest('table');
//     var rows = table.find('tr:gt(0)').toArray().sort($(this).index());
//    // console.log(table);
//     this.asc;

// };

            
    // $(".JSdate_time").trigger('onload', function () {
    
  
    // var table = $(this).closest('table');
    // var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));

    // this.asc = !this.asc;
    
    // if (!this.asc){ rows = rows.reverse(); }
    // for (var i = 0; i < rows.length; i++){ table.append(rows[i]) }  
           
    // }); 
       

function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index);
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB);
    }

}
function getCellValue(row, index){ return $(row).children('td').eq(index).text().trim().toLowerCase(); }



// END
// $(document).ready(function(e) {
//     var table = $('.JSdate_time').closest('table');
//     var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
 
   
    
//     if (!this.asc){ rows = rows.reverse(); }
   


// function comparer(index) {
//     return function(a, b) {
//         var valA = getCellValue(a, index), valB = getCellValue(b, index);
//         return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB);
//     }
// }




// function getCellValue(row, index){ return $(row).children('td').eq(index).text().trim().toLowerCase(); }
// })
// // END

// $(document).ready(function(e) {
// var ended= ('.end-flag').value();
// var dataRows = [];
// console.log(ended);
//   $('tr').each(function(i,j) {
//     dataRows.push({'crmDate': $(this).find('.crmDate').text().slice(0, -9).replace(/-/g, ''), 'row': $(this)});
//   })
//  ;
//   //Sort 
//   dataRows.sort(function(a, b) {
//    return a.crmDate - b.crmDate;
//   });
//  // console.log(dataRows);
//   //Remove existing table rows.  This assumes that everything should be deleted, adjust selector if needed :).
//   $('#JSCrmTable').empty();

//   //Add rows back to table in the correct order.
//   dataRows.forEach(function(ele) {
//     $('#JSCrmTable').append(ele.row);
//   })

// })

// function crmSelect()
// {
//     var index,
//     table = document.getElementById('JSmyTable');
//     for(var i = 0; i < table.rows.length; i++){
//         table.rows[i].onclick = function(){
//             if(typeof index !== 'undefined')
//             {
//                 table.rows[index].classList.toggle('selected');
//             }
//         index = this.rowIndex;
//         this.classList.toggle('selected');
//         //console.log(index);
//         }
//     }
// }
// crmSelect()


