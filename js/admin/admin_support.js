// $('.new-question_li').hide();
// $('.new-answer_li').hide();

$(document).ready(function () {

	$( ".JSeditSupport" ).change(function(){
		var url = $(this).val();
		if(url != ''){
			location.href = url;
		}
	});


	$( ".JSSaveKonf" ).click(function(){
		var konfigurator_id = $('#JSTabelaKonf').data('id');
		var old_grupa_pr_id = $(this).data('id');
		var grupa_pr_id = $(this).closest('.inline-list').find('.JSgrupa_pr_id').val();

		var obj_rbr = $(this).closest('.inline-list').find('.JSRbr');
		var rbr = obj_rbr.data('id');
		if(rbr != 'new'){
			rbr = obj_rbr.val();
		}
		
		$.post(base_url+'admin/ajax/support', {action: 'save_conf', konfigurator_id: konfigurator_id, old_grupa_pr_id: old_grupa_pr_id, grupa_pr_id: grupa_pr_id, rbr: rbr}, function (response){
			location.reload(true);
		});
	});

	$( ".JSDeleteKonf" ).click(function(){
		var konfigurator_id = $('#JSTabelaKonf').data('id');
		var grupa_pr_id = $(this).data('id');
		
		$.post(base_url+'admin/ajax/support', {action: 'delete_conf', konfigurator_id: konfigurator_id, grupa_pr_id: grupa_pr_id}, function (response){
			location.reload(true);
		});
	});

	$(".search_select").select2({
		width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    });
	

// POLLS
 	$('.JSeditpoll, .JSadd_question').on('click', function(){
 		$(this).parent().siblings('form').toggleClass('hide');
 	});

	$(function() {
		$('#JSPollQuestionSortable').sortable({
			update: function(event, ui) {

				var order = [];
				$('#JSPollQuestionSortable .JSPollQuestionSort').each( function(e) {
					order.push( $(this).attr('data-id') );
				});

				$.ajax({
					type: "POST",
					url: base_url+'admin/poll-question-position',
					data: { order:order },
					success: function(msg) {

					}
				});
			}
		});					   
		$( "#JSPollQuestionSortable").disableSelection();			
	});
	$(function() {
		$('#JSPollAnswerSortable').sortable({
			update: function(event, ui) {

				var order = [];
				$('#JSPollAnswerSortable .JSPollAnswerSort').each( function(e) {
					order.push( $(this).attr('data-id') );
				});

				$.ajax({
					type: "POST",
					url: base_url+'admin/poll-answer-position',
					data: { order:order },
					success: function(msg) {

					}
				});
			}
		});					   
		$( "#JSPollQuestionSortable").disableSelection();			
	});

});