 var base_url = $('#base_url').val();

 function translate(neki_string) {
         return neki_string;
     }

 $(document).ready(function() {
     /* Tamara html dodato */
     HTMLElement.prototype.click = function() {
         var evt = this.ownerDocument.createEvent('MouseEvents');
         evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
         this.dispatchEvent(evt);
     }
     var base_url = $('#base_url').val();

     // $('#admin-login').click(function() {

     //     var username = $('#username').val();
     //     var password = $('#password').val();
     //     if (username == '' || username == ' ') {
     //         $('#username').css("border", "1px solid red").focus().fadeIn(200);
     //     } else if (password == '' || password == ' ') {
     //         $('#password').css("border", "1px solid red").focus().fadeIn(200);
     //     } else {

     //         $.ajax({
     //             type: "POST",
     //             url: base_url + 'admin_login',
     //             data: { username: username, password: password },
     //             success: function(msg) {

     //                 switch (msg) {
     //                     case "1":
     //                         // popupOpen();
     //                         // setTimeout(function() {
     //                         //     popupClose();
     //                         // }, 2000);
     //                         // $('.info-popup .popup-inner').html("<div class='popup-close'><i class='fa fa-times'></i></div> <p class='p-info'>Korisnik ili lozinka su pogrešni.</p>");
     //                         // break;
     //                        alert('Korisnik ili lozinka su pogrešni!');
     //                     case "2":
     //                         // popupOpen();
     //                         // setTimeout(function() {
     //                         //     popupClose();
     //                         // }, 2000);
     //                         // $('.info-popup .popup-inner').html(" <div class='popup-close'></div> <p class='p-info'>Vaš nalog je suspendovan.</p>");
     //                         // break;
     //                         alert('Vaš nalog je suspendovan!');
     //                     case "0":
     //                         window.location.href = base_url + 'admin';
     //                         break;
     //                 }
     //             }
     //         });
     //     }

     //     // return false;
     // });

     function popupOpen() {
         $('.info-popup').addClass('popup-opened');
     }

     function popupClose() {
         $('.info-popup').removeClass('popup-opened');
     }
    
    $(".search_select").select2({
        width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    });

     $('.page-edit-save').click(function() {
         var naslov = $('#page_name').val();
         if (naslov == '' || naslov == ' ') {
             $('#page_name').css("border", "1px solid red").focus().fadeIn(200);
         } else {

             form_pages.submit();
         }

     });

     $('.JSosobine').on('change', function(){
        var osobina_naziv_id = $(this).find(':selected').data('id');
        var roba_id = $(this).find(':selected').data('roba');
        if( osobina_naziv_id !=null )
        location.href = base_url + 'admin/product_osobine/'+ roba_id + '/' + osobina_naziv_id;
        else{
        location.href = base_url + 'admin/product_osobine/'+ roba_id + '/0' ;
        }
    });

     $('.logo-upload-confirm').click(function() {
         logo_upload.submit();
     });

     $('.social').change(function() {
         var vrednost = $(this).val();
         var polje = $(this).data('naziv');
         // alert(vrednost+" "+polje);
         $.ajax({
             type: "POST",
             url: base_url + 'admin/social_update',
             data: { vrednost: vrednost, polje: polje }
         });

     });

     $('.banner-edit-save').click(function() {
         var naziv = $("#naziv").val();
         var link = $("#link").val();
         if (naziv == '' || naziv == ' ') {
             $('#naziv').css("border", "1px solid red").focus().fadeIn(200);
         } else if (link == '' || link == ' ') {
             $('#link').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             banner_upload.submit();
         }


     });

     $('.JSWebOptions').click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        $(this).addClass('active');
        $(this).siblings('.JSWebOptions').removeClass('active');

        if(id==203){
            if(id==203 && $(this).attr('id') == 'JSPrikazDa'){
                $('.JSOptionPrikazSifra').removeAttr('disabled');
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/setings_update',
                    data: { id: id, status: status },
                });
            }else if(id==203 && $(this).attr('id') == 'JSPrikazNe'){
                $('.JSOptionPrikazSifra').attr('disabled', 'disabled');
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/setings_update',
                    data: { id: id, status: status },
                });
                
            }
        }else{
            $.ajax({
                type: "POST",
                url: base_url + 'admin/setings_update',
                data: { id: id, status: status },
            });
        }
         });
    
     $('.JSOptions').click(function() {
         $(this).addClass('active');
         $(this).siblings('.JSOptions').removeClass('active');
         var id = $(this).data('id');
         var status = $(this).data('status');
         // alert(base_url+'admin/setings_update');
         $.ajax({
             type: "POST",
             url: base_url + 'admin/options_setings_update',
             data: { id: id, status: status },

         });
     });

   $('.JSOptionsChangeStrData').on('change',function () {
        var value = $(this).val();
        var id = $(this).data('id');

        $.post(base_url+'admin/options-change-str-data', {id: id, value: value}, function (response){});

   });

     $('.delete').click(function() {
         var baneri_id = $(this).data('id');
         $.ajax({
             type: "POST",
             url: base_url + 'admin/baneri_delete',
             data: { baneri_id: baneri_id },
             success: function() {
                 // alert(msg);
                 window.location.href = base_url + 'admin/baneri-slajdovi/new-banner';
             }
         });
     });

     $('.images-delete').click(function() {
         var url = $(this).data('url');

         //alert("Link je"+url);

         $.ajax({
             type: "POST",
             url: base_url + 'admin/delete_image',
             data: { url: url },
             success: function(msg) {
                 //alert(msg);
                 location.reload();
             }
         });
     });

     $('#shpmania_btn').click(function() {
         shopmania.submit();
     });
     $('#eponuda_btn').click(function() {
         eponuda.submit();
     });
     //end document
 });

 function check_fileds(polje) {
     polje2 = document.getElementById(polje).value;
     if (polje == 'email') {
         if (isValidEmailAddress(polje2) == false) {

             $('#email').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#email').css("border", "1px solid #ccc").fadeIn(200);
         }

     } else if (polje == 'without-reg-email') {
         if (isValidEmailAddress(polje2) == false) {

             $('#without-reg-email').css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#without-reg-email').css("border", "1px solid #ccc").fadeIn(200);
         }

     } else {
         if (polje2 == '' || polje2 == ' ') {
             $('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
         } else {
             $('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
         }

     }

 }

function character_limit(e,limit=320) {
    // document.onkeydown = function(){
        var value = e.target.value.length;

        if(value >= limit){
            if(value == limit && e.keyCode == 8){
                return true;
            }else{
                return false;
            }
        }
    // }
}

 function isValidEmailAddress(emailAddress) {
     var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
     return pattern.test(emailAddress);
 };

 //Dozvoljava samo numeric values
 function isNumberKey(charCode) {
     if (charCode > 31 && (charCode < 48 || charCode > 57))
         return false;
     return true;
 }
