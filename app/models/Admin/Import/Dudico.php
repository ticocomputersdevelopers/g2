<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Dudico {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/dudico/dudico_xml/dudico.xml');
			$products_file = "files/dudico/dudico_xml/dudico.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);
	        
			foreach ($products as $product):

				$images = $product->xpath('slike/slika');
				$flag_slika_postoji = "0";
				$i=0;
				foreach ($images as $slika):
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->sifra_artikla).",'".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->sifra_artikla).",'".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
				endforeach;

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra_artikla . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->naziv)) ." ( ". $product->sifra_artikla ." )"."',";
				$sPolja .= "grupa,";					$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->grupa)) . "',";
				$sPolja .= "model,";					$sVrednosti .= "'" . Support::encodeTo1250($product->model) . "',";
				$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($product->proizvodjac)) . "',";
				if(isset($product->kolicicna)){
				$sPolja .= "kolicina,";					$sVrednosti .= " 1,";
				}else{
				$sPolja .= "kolicina,";					$sVrednosti .= " 0,";
				}
				if(isset($product->opis)){
				$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";
				}else{
				$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 0,";
				}
				$sPolja .= " opis,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($product->opis)) . "',";
				$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " '" . $flag_slika_postoji . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->cena_nc . ",";
				$sPolja .= "web_cena,";					$sVrednosti .= "" . $product->web_cena . ",";
				$sPolja .= "mpcena";					$sVrednosti .= "" . $product->mpcena . "";
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
			

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/dudico/dudico_xml/dudico.xml');
			$products_file = "files/dudico/dudico_xml/dudico.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

		$products = simplexml_load_file($products_file);
	        
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra_artikla . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->cena_nc . ",";
				if(isset($product->kolicicna)){
				$sPolja .= "kolicina,";					$sVrednosti .= " 1,";
				}else{
				$sPolja .= "kolicina,";					$sVrednosti .= " 0,";
				}
				$sPolja .= "web_cena,";					$sVrednosti .= "" . $product->web_cena . ",";
				$sPolja .= "mpcena";					$sVrednosti .= "" . $product->mpcena . "";

		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}