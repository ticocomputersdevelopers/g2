<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Keno {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/keno/keno_excel/keno.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
			$niz_sifara = array();
			for ($row = 1; $row <= $lastRow; $row++) {
				if(!empty($worksheet->getCell('A'.$row)->getValue())){
					foreach(explode(' ',trim($worksheet->getCell('A'.$row)->getValue())) as $sifra1){
						if(!empty($sifra1)){
							if(!in_array($sifra1, $niz_sifara)){

								$niz_sifara[]=$sifra1;

								$naziv = '';
								$model = '';
								
								for ($row2 = 1; $row2 <= $lastRow; $row2++) {
									foreach(explode(' ',trim($worksheet->getCell('A'.$row2)->getValue())) as $sifra2){
										if($sifra2 == $sifra1){
											if(!empty($naziv)){
												$naziv = $naziv.', '.$worksheet->getCell('C'.$row2)->getValue();
											}else{
												$naziv .= $worksheet->getCell('C'.$row2)->getValue();
											}

											if(!empty($model)){
												$model = $model.', '.$worksheet->getCell('B'.$row2)->getValue();
											}else{
												$model .= $worksheet->getCell('B'.$row2)->getValue();
											}
										}
									}
								}

								
								$sPolja = '';
								$sVrednosti = '';
								$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
								$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra1) . "',";
							
								$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250(ucfirst(mb_strtolower($naziv.' '.$model)) . " ( " . $sifra1)) . " )',";
								
								$sPolja .= " model,";					$sVrednosti .= " '" . addslashes($model) . "',";
								$sPolja .= " barkod,";					$sVrednosti .= " '" . addslashes($sifra1) . "',";
								$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
								$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($worksheet->getCell('D'.$row)->getValue()),2,$kurs,$valuta_id_nc),2, '.', '') . "";

								DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
							}
						}
					}	
				}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/keno/keno_excel/keno.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

			$niz_sifara = array();
			for ($row = 1; $row <= $lastRow; $row++) {
				if(!empty($worksheet->getCell('A'.$row)->getValue())){
					foreach(explode(' ',trim($worksheet->getCell('A'.$row)->getValue())) as $sifra1){
						if(!empty($sifra1)){
							if(!in_array($sifra1, $niz_sifara)){
								$niz_sifara[]=$sifra1;

								$sPolja = '';
								$sVrednosti = '';
								$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
								$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra1) . "',";
								$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
								$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($worksheet->getCell('D'.$row)->getValue()),2,$kurs,$valuta_id_nc),2, '.', '') . "";

								DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
							}
						}
					}	
				}
			}


			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}