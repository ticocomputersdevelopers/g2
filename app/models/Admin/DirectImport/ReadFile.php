<?php
namespace DirectImport;

use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class ReadFile {

	public static function xml(){
		$file_path = "files/direct_import.xml";
		$success = false;
		$articles = array();
		if(File::exists($file_path)){
			$products = simplexml_load_file($file_path);
			foreach($products as $product){
				if(isset($product->sifra_artikla) 
					// && isset($product->naziv) 
					// && isset($product->web_cena) && is_numeric(floatval($product->web_cena))
					){ 
					$article = (object) array(
						'code' => trim((string) $product->sifra_artikla),
						'barcode' => isset($product->barkod) ? trim((string) $product->barkod) : '',
						'name' => trim((string) $product->naziv),
						'tax' => isset($product->pdv) ? trim((string) $product->pdv) : '20',
						'group' => isset($product->grupa) ? trim((string) $product->grupa) : '-1',
						'manufacturer' => isset($product->proizvodjac) ? trim((string) $product->proizvodjac) : '-1',
						'model' => isset($product->model) ? trim((string) $product->model) : '',
						'quantity' => isset($product->kolicina) && is_numeric(intval($product->kolicina)) ? trim((string) $product->kolicina) : '0',
						'purchase_price' => isset($product->cena_nc) && is_numeric(floatval($product->cena_nc)) ? trim((string) $product->cena_nc) : '0',
						'retail_price' => isset($product->mpcena) && is_numeric(floatval($product->mpcena)) ? trim((string) $product->mpcena) : '0',
						'web_price' => trim((string) $product->web_cena),
						'description' => isset($product->opis) ? trim((string) $product->opis) : '',
						'images' => array(),
						'characteristics_groups' => array()
						);
					if(isset($product->slike) && isset($product->slike->slika)){
						foreach((array) $product->slike->slika as $image){
							$article->images[] = $image;
						}
					}
					foreach($product->xpath('karakteristike/karakteristike_grupa') as $characteristics_group){
						$characteristics_groups = (object) array('name'=> (string) $characteristics_group->attributes()->ime, 'characteristics'=>array());
						foreach($characteristics_group->karakteristika as $characteristic){
							$characteristics_groups->characteristics[] = (object) array('name'=> (string) $characteristic->attributes()->ime, 'value'=> (string) $characteristic->vrednost);
						}
						$article->characteristics_groups[] = $characteristics_groups;
						
					}

					$articles[] = $article;
				}
			}
			if(count($articles) > 0){
				$success = true;
			}
		}
		return (object) array('articles'=>$articles, 'success'=>$success);
	}

	public static function xls($xlsx=false){
		$file_path = !$xlsx ? "files/direct_import.xls" : "files/direct_import.xlsx";
		$success = false;
		$articles = array();
		if(File::exists($file_path)){

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {

				if(!is_null($worksheet->getCell('A'.$row)->getValue()) 
					// && !is_null($worksheet->getCell('C'.$row)->getValue()) 
					// && !is_null($worksheet->getCell('K'.$row)->getValue()) && is_numeric($worksheet->getCell('K'.$row)->getValue())
					){ 
					$article = (object) array(
						'code' => trim((string) $worksheet->getCell('A'.$row)->getValue()),
						'barcode' => !is_null($worksheet->getCell('B'.$row)->getValue()) ? trim((string) $worksheet->getCell('B'.$row)->getValue()) : '',
						'name' => trim((string) $worksheet->getCell('C'.$row)->getValue()),
						'tax' => !is_null($worksheet->getCell('D'.$row)->getValue()) ? trim((string) $worksheet->getCell('D'.$row)->getValue()) : '20',
						'group' => !is_null($worksheet->getCell('E'.$row)->getValue()) ? trim((string) $worksheet->getCell('E'.$row)->getValue()) : '-1',
						'manufacturer' => !is_null($worksheet->getCell('F'.$row)->getValue()) ? trim((string) $worksheet->getCell('F'.$row)->getValue()) : '-1',
						'model' => !is_null($worksheet->getCell('G'.$row)->getValue()) ? trim((string) $worksheet->getCell('G'.$row)->getValue()) : '',
						'quantity' => !is_null($worksheet->getCell('H'.$row)->getValue()) && is_numeric(intval($worksheet->getCell('H'.$row)->getValue())) ? trim((string) $worksheet->getCell('H'.$row)->getValue()) : '0',
						'purchase_price' => !is_null($worksheet->getCell('I'.$row)->getValue()) && is_numeric($worksheet->getCell('I'.$row)->getValue()) ? trim((string) $worksheet->getCell('I'.$row)->getValue()) : '0',
						'retail_price' => !is_null($worksheet->getCell('J'.$row)->getValue()) && is_numeric($worksheet->getCell('J'.$row)->getValue()) ? trim((string) $worksheet->getCell('J'.$row)->getValue()) : '0',
						'web_price' => trim((string) $worksheet->getCell('K'.$row)->getValue()),
						'description' => !is_null($worksheet->getCell('L'.$row)->getValue()) ? trim((string) $worksheet->getCell('L'.$row)->getValue()) : '',
						'images' => array(),
						'characteristics_groups' => array()
						);
					if(!is_null($worksheet->getCell('M'.$row)->getValue())){
						$article->images[] = trim((string) $worksheet->getCell('M'.$row)->getValue());
					}
					if(!is_null($worksheet->getCell('N'.$row)->getValue())){
						$article->images[] = trim((string) $worksheet->getCell('N'.$row)->getValue());
					}
					if(!is_null($worksheet->getCell('O'.$row)->getValue())){
						$article->images[] = trim((string) $worksheet->getCell('O'.$row)->getValue());
					}
					if(!is_null($worksheet->getCell('P'.$row)->getValue())){
						$article->images[] = trim((string) $worksheet->getCell('P'.$row)->getValue());
					}

					$exists_characts = false;
					$characteristics_groups = (object) array('name'=> 'Osnovne karakteristike', 'characteristics'=>array());
					if(!is_null($worksheet->getCell('Q'.$row)->getValue()) && !is_null($worksheet->getCell('R'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('Q'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('R'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('S'.$row)->getValue()) && !is_null($worksheet->getCell('T'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('S'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('T'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('U'.$row)->getValue()) && !is_null($worksheet->getCell('V'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('U'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('V'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('W'.$row)->getValue()) && !is_null($worksheet->getCell('X'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('W'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('X'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('Y'.$row)->getValue()) && !is_null($worksheet->getCell('Z'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('Y'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('Z'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('AA'.$row)->getValue()) && !is_null($worksheet->getCell('AB'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('AA'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('AB'.$row)->getValue()));
						$exists_characts = true;
					}
					if(!is_null($worksheet->getCell('AC'.$row)->getValue()) && !is_null($worksheet->getCell('AD'.$row)->getValue())){
						$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $worksheet->getCell('AC'.$row)->getValue()), 'value'=> trim((string) $worksheet->getCell('AD'.$row)->getValue()));
						$exists_characts = true;
					}
					if($exists_characts){
						$article->characteristics_groups[] = $characteristics_groups;
					}

					$articles[] = $article;
				// var_dump($article);die;
				}
			}
			if(count($articles) > 0){
				$success = true;
			}
		}
		return (object) array('articles'=>$articles, 'success'=>$success);
	}

	public static function csv(){
		$file_path = "files/direct_import.csv";
		$success = false;
		$articles = array();
		if(File::exists($file_path)){

			if (($handle = fopen($file_path, "r")) !== FALSE) {
			    while (($product = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    	
					if(isset($product[0]) 
						&& isset($product[2]) && $product[2] != '' 
						&& isset($product[10]) && $product[10] != '' && is_numeric($product[10])
						){ 
						$article = (object) array(
							'code' => trim((string) $product[0]),
							'barcode' => isset($product[1]) && $product[1] != '' ? trim((string) $product[1]) : '',
							'name' => trim((string) $product[2]),
							'tax' => isset($product[3]) && $product[3] != '' ? trim((string) $product[3]) : '20',
							'group' => isset($product[4]) && $product[4] != '' ? trim((string) $product[4]) : '-1',
							'manufacturer' => isset($product[5]) && $product[5] != '' ? trim((string) $product[5]) : '-1',
							'model' => isset($product[6]) && $product[6] != '' ? trim((string) $product[6]) : '',
							'quantity' => isset($product[7]) && $product[7] != '' && is_numeric(intval($product[7])) ? trim((string) $product[7]) : '0',
							'purchase_price' => isset($product[8]) && $product[8] != '' && is_numeric($product[8]) ? trim((string) $product[8]) : '0',
							'retail_price' => isset($product[9]) && $product[9] != '' && is_numeric($product[9]) ? trim((string) $product[9]) : '0',
							'web_price' => trim((string) $product[10]),
							'description' => isset($product[11]) && $product[11] != '' ? trim((string) $product[11]) : '',
							'images' => array(),
							'characteristics_groups' => array()
							);
						if(isset($product[12]) && $product[12] != ''){
							$article->images[] = trim((string) $product[12]);
						}
						if(isset($product[13]) && $product[13] != ''){
							$article->images[] = trim((string) $product[13]);
						}
						if(isset($product[14]) && $product[14] != ''){
							$article->images[] = trim((string) $product[14]);
						}
						if(isset($product[15]) && $product[15] != ''){
							$article->images[] = trim((string) $product[15]);
						}

						$exists_characts = false;
						$characteristics_groups = (object) array('name'=> 'Osnovne karakteristike', 'characteristics'=>array());
						if(isset($product[16]) && $product[16] != '' && isset($product[17]) && $product[17] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[16]), 'value'=> trim((string) $product[17]));
							$exists_characts = true;
						}
						if(isset($product[18]) && $product[18] != '' && isset($product[19]) && $product[19] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[18]), 'value'=> trim((string) $product[19]));
							$exists_characts = true;
						}
						if(isset($product[20]) && $product[20] != '' && isset($product[21]) && $product[21] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[20]), 'value'=> trim((string) $product[21]));
							$exists_characts = true;
						}
						if(isset($product[22]) && $product[22] != '' && isset($product[23]) && $product[23] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[22]), 'value'=> trim((string) $product[23]));
							$exists_characts = true;
						}
						if(isset($product[24]) && $product[24] != '' && isset($product[25]) && $product[25] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[24]), 'value'=> trim((string) $product[25]));
							$exists_characts = true;
						}
						if(isset($product[26]) && $product[26] != '' && isset($product[27]) && $product[27] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[26]), 'value'=> trim((string) $product[27]));
							$exists_characts = true;
						}
						if(isset($product[28]) && $product[28] != '' && isset($product[29]) && $product[29] != ''){
							$characteristics_groups->characteristics[] = (object) array('name'=> trim((string) $product[28]), 'value'=> trim((string) $product[29]));
							$exists_characts = true;
						}
						if($exists_characts){
							$article->characteristics_groups[] = $characteristics_groups;
						}

						$articles[] = $article;
					}
				}
				fclose($handle);
			}
			if(count($articles) > 0){
				$success = true;
			}
		}
		return (object) array('articles'=>$articles, 'success'=>$success);
	}
	public static function xls_partner($xlsx=false){
		$file_path = !$xlsx ? "files/partner_import.xls" : "files/partner_import.xlsx";
		$success = false;
		$partners = array();
		if(File::exists($file_path)){

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($file_path);
	        $excelObj = $excelReader->load($file_path);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {

					$partner = (object) array(
						'naziv' => trim( $worksheet->getCell('A'.$row)->getValue()),
						'adresa' => trim( $worksheet->getCell('B'.$row)->getValue()),
						'mesto' => trim( $worksheet->getCell('C'.$row)->getValue()),
						'telefon' => trim( $worksheet->getCell('D'.$row)->getValue()),
						'fax' => trim( $worksheet->getCell('E'.$row)->getValue()),
						'pib' => trim( $worksheet->getCell('F'.$row)->getValue()),
						'broj_maticni' => trim( $worksheet->getCell('G'.$row)->getValue()),
						'mail' => trim( $worksheet->getCell('H'.$row)->getValue()),
						'komercijalista' =>trim( $worksheet->getCell('I'.$row)->getValue())
						);

					$partners[] = $partner;
				//var_dump($partner);die;
			}
			if(count($partners) > 0){
				$success = true;
			}
		}
		return (object) array('partners'=>$partners, 'success'=>$success);
	}

}