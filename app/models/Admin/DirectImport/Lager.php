<?php
namespace DirectImport;
use DB;

class Lager {

	public static function table_body($articles){

		$result_arr = array();
		$roba_id = -1;
		foreach($articles as $article) {
    		$roba_id--;

			$sifra = pg_escape_string($article->code);
			$kolicina = intval($article->quantity);
			if($kolicina < 0){
				$kolicina = 0;
			}

			$result_arr[] = "(".strval($roba_id).",1,0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		

		DB::statement("DELETE from lager WHERE roba_id in (SELECT roba_id from roba where sifra_is in (SELECT sifra_is from ".$table_temp." where kolicina > 0)) and (sifra_is is null or sifra_is = '') and orgj_id = 1 and poslovna_godina_id = 2014");
		DB::statement("DELETE FROM lager WHERE roba_id < 0");
		
		foreach($columns as $col){
				DB::statement("UPDATE lager t SET ".$col." = lager_temp.".$col." FROM ".$table_temp." WHERE t.sifra_is=lager_temp.sifra_is::varchar and lager_temp.".$col." > 0");
		}
		//insert		
		DB::statement("INSERT INTO lager (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM lager t WHERE t.sifra_is=lager_temp.sifra_is::varchar) and lager_temp.sifra_is in (SELECT sifra_is from roba))");

		DB::statement("DELETE FROM lager WHERE sifra_is = (SELECT sifra_is FROM lager GROUP BY sifra_is HAVING COUNT(sifra_is) > 1)");
		DB::statement("UPDATE lager l SET roba_id = roba.roba_id FROM roba WHERE l.sifra_is=roba.sifra_is::varchar and l.kolicina > 0");
		DB::statement("DELETE FROM lager WHERE roba_id < 0");

	}

}