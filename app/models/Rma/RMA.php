<?php

class RMA {

	public static function getRadniNalozi($datum_od=null,$datum_do=null,$search=null,$radni_nalog_status_id=null,$otpis=null,$ostecen=null,$partner_ids=array(),$serviser_ids=array(),$sort_column='radni_nalog_id',$sort_direction='ASC',$limit=30,$page=1){

		$query = "SELECT broj_naloga, serijski_broj, kupac_id, b2b_partner_id, datum_prijema, datum_zavrsetka, uredjaj, opis_kvara, rn.radni_nalog_id as radni_nalog_id, broj_naloga, status_id, p.partner_id as partner_id, SUM(rno.iznos) AS iznos, partner_proizvodjac_id, datum_kupovine, maloprodaja, napomena_operacije, primio_serviser_id, datum_servisiranja, broj_fiskalnog_racuna, rn.napomena AS rn_napomena, uzrok_kvara, otpis, ostecen FROM radni_nalog rn LEFT JOIN web_kupac wk ON wk.web_kupac_id = rn.kupac_id LEFT JOIN partner p ON p.partner_id = rn.partner_id LEFT JOIN radni_nalog_operacije rno ON rno.radni_nalog_id = rn.radni_nalog_id";

		$where = false;
		if(!is_null($search) && $search != ''){
			$query .= " WHERE";
			$where = true;

			$search = urldecode($search);
			$query .= " (";
			foreach(explode(' ',$search) as $word){
				$query .= " broj_naloga ILIKE '%".$word."%' OR wk.ime ILIKE '%".$word."%' OR wk.prezime ILIKE '%".$word."%' OR p.naziv ILIKE '%".$word."%' OR rn.uredjaj ILIKE '%".$word."%' OR rn.serijski_broj ILIKE '%".$word."%' OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}

		if(!is_null($datum_od)){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(rn.datum_prijema) > '".$datum_od."'";
		}
		if(!is_null($datum_do)){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(rn.datum_prijema) < '".$datum_do."'";
		}
		if(count($partner_ids) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}

			$query .= " (";
			foreach($partner_ids as $partner_id){
				$query .= " rn.partner_id = ".$partner_id." OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}
		if(count($serviser_ids) > 0){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}

			$query .= " (";
			foreach($serviser_ids as $serviser_id){
				$query .= " rn.primio_serviser_id = ".$serviser_id." OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
		}


		if(!is_null($radni_nalog_status_id)){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " rn.status_id = ".$radni_nalog_status_id."";
		}
		if(!is_null($otpis)){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " rn.otpis = ".$otpis."";
		}
		if(!is_null($ostecen)){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " rn.ostecen = ".$ostecen."";
		}

		if($user = RmaOptions::user('servis')){
			if(!$where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " p.partner_id = ".$user->partner_id."";
		}

		$query .= " GROUP BY (broj_naloga, serijski_broj, kupac_id, datum_prijema, datum_zavrsetka, uredjaj, opis_kvara, rn.radni_nalog_id, broj_naloga, status_id, p.partner_id, proizvodjac, datum_kupovine, maloprodaja, napomena_operacije)";
		$all = DB::select($query);
		$count = count($all);

		$query .= " ORDER BY ".$sort_column." ".$sort_direction."";
		$query .= " LIMIT ".strval($limit)." OFFSET ".strval($limit*($page-1))."";

		return (object) array('rows'=>DB::select($query), 'all' => $all, 'count' => $count);
	}

	public static function getRadniNalog($radni_nalog_id){		
		return DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->first();		
	}

	public static function getStatusTitle($status_id){		
		return DB::table('radni_nalog_status')->where('radni_nalog_status_id',$status_id)->pluck('naziv');
	}

	public static function getRadniNalogStatus($staus_id){
		return DB::table('radni_nalog_status')->where('radni_nalog_status_id',$staus_id)->first();
	}

	public static function statusSelect($radni_nalog_status_id=null){
		$render = '';
        foreach(DB::table('radni_nalog_status')->select('radni_nalog_status_id','naziv')->whereNotIn('radni_nalog_status_id',array(2,3))->orderBy('radni_nalog_status_id','asc')->get() as $row){

	            if($row->radni_nalog_status_id == $radni_nalog_status_id){
	                $render .= '<option value="'.$row->radni_nalog_status_id.'" selected>'.$row->naziv.'</option>';
	            }else{
	                $render .= '<option value="'.$row->radni_nalog_status_id.'">'.$row->naziv.'</option>';
	            }
        }
        return $render;
	}

	public static function servisSelect($partner_id=null){
		$partneri_query = DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->select('partner.partner_id','partner.naziv')->where('partner_je.partner_vrsta_id',118);
		if($user = RmaOptions::user('servis')){
			$partneri_query = $partneri_query->where('partner.partner_id',$user->partner_id);
		}
		$partneri = $partneri_query->orderBy('partner.partner_id','asc')->get();

		$render = '';
        foreach($partneri as $row){
        	if(count(DB::select("SELECT serviser_id FROM serviser WHERE partner_id = ".$row->partner_id."")) > 0){
	            if($row->partner_id == $partner_id){
	                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
	            }else{
	                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
	            }
	        }
        }
        return $render;
	}

	public static function servisMultiSelect($partner_ids=array()){
		$partneri_query = DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->select('partner.partner_id','partner.naziv')->where('partner_je.partner_vrsta_id',118);
		if($user = RmaOptions::user('servis')){
			$partneri_query = $partneri_query->where('partner.partner_id',$user->partner_id);
		}
		$partneri = $partneri_query->orderBy('partner.partner_id','asc')->get();

		$render = '';
        foreach($partneri as $row){
        	if(count(DB::select("SELECT serviser_id FROM serviser WHERE partner_id = ".$row->partner_id."")) > 0){
	            if(in_array($row->partner_id,$partner_ids)){
	                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
	            }else{
	                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
	            }
	        }
        }
        return $render;
	}

	public static function kupciSelect($web_kupac_id=null){
		$render = '';
        foreach(DB::table('web_kupac')->select('web_kupac_id','ime','prezime','naziv', 'flag_vrsta_kupca')->where('web_kupac_id','!=',-1)->get() as $row){

        	if($row->flag_vrsta_kupca == 0) {
	            if($row->web_kupac_id == $web_kupac_id){
	                $render .= '<option value="'.$row->web_kupac_id.'" selected>'.$row->ime.' '.$row->prezime.'</option>';
	            }else{
	                $render .= '<option value="'.$row->web_kupac_id.'">'.$row->ime.' '.$row->prezime.'</option>';
	            }
        	}
        }
        return $render;
	}

	public static function serviseriSelect($partner_id,$serviser_id=null){
		$render = '';
        foreach(DB::select("SELECT * FROM serviser WHERE partner_id = ".$partner_id." ORDER BY serviser_id ASC") as $row){
            if($row->serviser_id == $serviser_id){
                $render .= '<option value="'.$row->serviser_id.'" selected>'.$row->ime.' '.$row->prezime.'</option>';
            }else{
                $render .= '<option value="'.$row->serviser_id.'">'.$row->ime.' '.$row->prezime.'</option>';
            }
        }

        return $render;
	}

	public static function serviseriMultiSelect($partner_ids,$serviser_ids=array()){
		$render = '';
		if(count($partner_ids) > 0){
	        foreach(DB::select("SELECT * FROM serviser WHERE partner_id IN (".implode(',',$partner_ids).") ORDER BY serviser_id ASC") as $row){
	            if(in_array($row->serviser_id,$serviser_ids)){
	                $render .= '<option value="'.$row->serviser_id.'" selected>'.$row->ime.' '.$row->prezime.'</option>';
	            }else{
	                $render .= '<option value="'.$row->serviser_id.'">'.$row->ime.' '.$row->prezime.'</option>';
	            }
	        }
	    }

        return $render;
	}
	public static function uredjajiSelect($roba_ids=array(),$dodati=true){
		$query = DB::table('roba')->select('roba_id','naziv_web');
		if($dodati){
			$query = $query->whereIn('roba_id',array_map('current',DB::table('radni_nalog')->select('roba_id')->whereNotNull('roba_id')->get()));
		}
		$data = $query->orderBy('naziv_web','asc')->get();

		$render = '';
        foreach($data as $row){
            if(in_array($row->roba_id,$roba_ids)){
                $render .= '<option value="'.$row->roba_id.'" selected>'.$row->naziv_web.'</option>';
            }else{
                $render .= '<option value="'.$row->roba_id.'">'.$row->naziv_web.'</option>';
            }
        }
        return $render;
	}

	public static function rezervniDeoSelect($roba_ids=array()){
		$render = '';
        foreach(DB::table('roba')->select('roba_id','naziv_web')->whereIn('roba_id',array_map('current',DB::table('radni_nalog_rezervni_deo')->select('roba_id')->whereNotNull('roba_id')->get()))->get() as $row){
            if(in_array($row->roba_id,$roba_ids)){
                $render .= '<option value="'.$row->roba_id.'" selected>'.$row->naziv_web.'</option>';
            }else{
                $render .= '<option value="'.$row->roba_id.'">'.$row->naziv_web.'</option>';
            }
        }
        return $render;
	}

	public static function proizvodjacPartnerSelect($partner_id=null){
		$render = '<option value="null" selected>Izaberi</option>';
        foreach(DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->select('partner.partner_id','partner.naziv')->where('partner_je.partner_vrsta_id',120)->get() as $row){
            if($row->partner_id == $partner_id){
                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}

	public static function grupaOperacijeSelect($partner_id,$operacija_servis_id=null){
		$partner_id = 1;
		$operacija_grupa_id = null;
		if(!is_null($operacija_servis_id) && $operacija_servis_id > 0 && $operacija_servis_id != ''){
			$operacija_grupa = DB::select("SELECT (SELECT operacija_grupa_id FROM operacija WHERE operacija_id=os.operacija_id) AS operacija_grupa_id FROM operacija_servis os WHERE operacija_servis_id = ".$operacija_servis_id."");
			if(count($operacija_grupa) > 0){
				$operacija_grupa_id = $operacija_grupa[0]->operacija_grupa_id;
			}
		}

		$grupe = DB::select("SELECT DISTINCT operacija_grupa.operacija_grupa_id AS operacija_grupa_id, operacija_grupa.naziv AS naziv FROM operacija_servis LEFT JOIN operacija ON operacija.operacija_id = operacija_servis.operacija_id LEFT JOIN operacija_grupa ON operacija_grupa.operacija_grupa_id = operacija.operacija_grupa_id WHERE partner_id=".$partner_id." ORDER BY operacija_grupa.naziv ASC");

		$render = '';
        foreach($grupe as $row){
            if($row->operacija_grupa_id == $operacija_grupa_id){
                $render .= '<option value="'.$row->operacija_grupa_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->operacija_grupa_id.'">'.$row->naziv.'</option>';
            }
        }

        return $render;
	}

	public static function operacijeSelect($partner_id,$operacija_grupa_id=null,$operacija_servis_id=null){
		$partner_id = 1;
		if(is_null($operacija_grupa_id)){
			$operacija_grupa = DB::select("SELECT DISTINCT operacija_grupa.operacija_grupa_id AS operacija_grupa_id, operacija_grupa.naziv FROM operacija_servis LEFT JOIN operacija ON operacija.operacija_id = operacija_servis.operacija_id LEFT JOIN operacija_grupa ON operacija_grupa.operacija_grupa_id = operacija.operacija_grupa_id WHERE partner_id=".$partner_id." ORDER BY operacija_grupa.naziv ASC LIMIT 1");
			if(count($operacija_grupa)>0){
				$operacija_grupa_id = $operacija_grupa[0]->operacija_grupa_id;
			}else{
				return '';
			}
		}

		$operacija_id = null;
		if(!is_null($operacija_servis_id) && $operacija_servis_id > 0 && $operacija_servis_id != ''){
			$operacije = DB::select("SELECT os.operacija_id AS operacija_id, (SELECT operacija_grupa_id FROM operacija WHERE operacija_id=os.operacija_id) AS operacija_grupa_id FROM operacija_servis os WHERE operacija_servis_id = ".$operacija_servis_id."");
			if(count($operacije) > 0){
				$operacija_id = $operacije[0]->operacija_id;
				$operacija_grupa_id = $operacije[0]->operacija_grupa_id;
			}
		}

		$operacije = DB::select("SELECT DISTINCT operacija.operacija_id AS operacija_id, operacija.naziv AS naziv FROM operacija_servis LEFT JOIN operacija ON operacija.operacija_id = operacija_servis.operacija_id WHERE partner_id=".$partner_id." AND operacija.operacija_grupa_id = ".$operacija_grupa_id." ORDER BY operacija.naziv ASC");

		$render = '';
        foreach($operacije as $row){
            if($row->operacija_id == $operacija_id){
                $render .= '<option value="'.$row->operacija_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->operacija_id.'">'.$row->naziv.'</option>';
            }
        }

        return (object) array('select_content' => $render, 'first_operacija_id'=> (isset($operacije[0]) ? $operacije[0]->operacija_id : 0));
	}

	public static function usluzniServisSelect($partner_id=null){

		$render = '';
        // foreach(DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->select('partner.partner_id','partner.naziv')->where('partner_je.partner_vrsta_id',119)->get() as $row){
		foreach(DB::table('partner')->select('partner_id','naziv')->where('partner_id','>',0)->get() as $row){
            if($row->partner_id == $partner_id){
                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}

	public static function partnerDetails($partner_id){
		$render = '';
		if($partner_id > 0){
			$partner = DB::table('partner')->where('partner_id',$partner_id)->first();
			$render .= '<p>
			    	PIB: '. $partner->pib .'<br>
					Adresa: ' .$partner->adresa.', '.$partner->mesto.'<br>
					E-mail: ' .$partner->mail .'<br>
					Telefon: ' .$partner->telefon .
					'</p>';
		}
        return $render;
	}

	public static function kupacDetails($kupac_id){
		$kupac = DB::table('web_kupac')->where('web_kupac_id',$kupac_id)->first();
		if(is_null($kupac)){
			return '';
		}
		$render = '<p>
				Adresa: ' .$kupac->adresa.', '.$kupac->mesto.'<br>
				E-mail: ' .$kupac->email .'<br>
				Telefon: ' .$kupac->telefon .' '.$kupac->telefon_mobilni.
				'</p>';
        return $render;
	}

	public static function radni_nalog_null($kupac_id,$partner_id){
		return (object) array(
              'radni_nalog_id' => 0,
              'partner_id' => $partner_id,
              'kupac_id' => $kupac_id,
              'primio_serviser_id' => $partner_id > 0 ? DB::table('serviser')->where('partner_id',$partner_id)->orderBy('serviser_id','asc')->pluck('serviser_id') : null,
              'predao_serviser_id' => null,
              // 'broj_naloga' => 'RN'.(DB::table("radni_nalog")->max('radni_nalog_id')+1),
              'broj_naloga' => 'Novi radni nalog',
              'datum_prijema' => date('Y-m-d H:i'),
              'datum_zavrsetka' => null,
              'napomena' => null,
              'uredjaj' => null,
              'opis_kvara' => null,
              'roba_id' => null,
              'serijski_broj' => null,
              'preuzeo_ime' => null,
              'preuzeo_brojlk' => null,
              'status_id' => 1,
              'predat_uredjaj' => null,
              'napomena_operacije' => null,
              'datum_servisiranja' => null,
              'partner_proizvodjac_id' => null,
              'datum_kupovine' => null,
              'broj_fiskalnog_racuna' => null,
              'maloprodaja'=>null,
              'proizvodjac'=>null,
              'b2b_partner_id' => 0,
              'vrsta_kupca'=>'b2c'
            );
	}

	public static function operacijeServisSelect($partner_id,$operacija_servis_id=null){
		$operacije = DB::table('operacija_servis')->select('operacija_servis_id','operacija_grupa.naziv AS grupa_naziv','operacija.naziv AS naziv')->leftJoin('operacija','operacija.operacija_id','=','operacija_servis.operacija_id')->leftJoin('operacija_grupa','operacija_grupa.operacija_grupa_id','=','operacija.operacija_grupa_id')->where('partner_id',$partner_id)->orderBy('operacija_grupa.naziv','ASC')->orderBy('operacija.naziv','ASC')->get();
		$render = '';
        foreach($operacije as $row){
            if($row->operacija_servis_id == $operacija_servis_id){
                $render .= '<option value="'.$row->operacija_servis_id.'" selected>'.$row->grupa_naziv.' -> '.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->operacija_servis_id.'">'.$row->grupa_naziv.' -> '.$row->naziv.'</option>';
            }
        }

        return $render;
	}
	
	public static function radni_nalog_cene($radni_nalog_id,$itrosak=true){
        // $sum_cena_rada = DB::select("SELECT SUM(cena_sata*norma_sat) AS cena_rada FROM radni_nalog_operacije WHERE radni_nalog_id=".$id."");
        // $cena_rada = isset($sum_cena_rada[0]) ? $sum_cena_rada[0]->cena_rada : 0;
        $ukupan_iznos_rada = DB::table('radni_nalog_operacije')->where('radni_nalog_id',$radni_nalog_id)->sum('iznos');
        // $cena_rada = $ukupan_iznos_rada / 1.2;
        $cena_rada = $ukupan_iznos_rada;
        $ukupna_cena_rada = $cena_rada;

        $rezervni_delovi = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_id',$radni_nalog_id)->sum('iznos');
        $ukupna_cena_rada += $rezervni_delovi;

        $troskovi = 0;
        if($itrosak){
	        $troskovi = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$radni_nalog_id)->sum('vrednost_racuna');
	        $ukupna_cena_rada += $troskovi;
	    }

        $pdv_cena = $ukupna_cena_rada*0.2;
        $ukupno = $ukupna_cena_rada*1.2;

        return (object) array(
            'cena_rada' => $cena_rada,
            'rezervni_delovi' => $rezervni_delovi,
            'troskovi' => $troskovi,
            'ukupna_cena_rada' => $ukupna_cena_rada,
            'pdv_cena' => $pdv_cena,
            'ukupno' => $ukupno
        );		
	}
	public static function format_cene($cena){
		return number_format($cena,2,",",".");
	}
	public static function format_datuma($datum,$with_time=false){
		if(is_null($datum) || $datum == ''){
			return '';
		}
		$datum = date_create($datum);
		if($with_time){
			return date_format($datum, 'd.m.Y H:i:s');
		}else{
			return date_format($datum, 'd.m.Y');
		}
	}

    public static function serviser($serviser_id){
        return DB::table('serviser')->where(array('serviser_id'=> $serviser_id))->first();
    }

	public static function find($roba_id,$column,$magacin_id=null){
		
		if($column == 'kolicina' || $column == 'orgj_id' || $column == 'poslovna_godina_id'){
			$godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
			$default_magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
			if($magacin_id==0){
				if($column == 'kolicina'){
					$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id))->sum('kolicina');
				}elseif($column == 'orgj_id'){
					$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id))->pluck('orgj_id');
				}elseif($column == 'poslovna_godina_id'){
					$result = $godina_id;
				}

			}elseif($magacin_id!=null){
				$default_magacin_id = $magacin_id;
				$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>$default_magacin_id))->pluck($column);
			}
			if(is_null($result)){
				if($column=='kolicina'){
					$result = 0;
				}
				if($column=='orgj_id'){
					$result = $default_magacin_id;
				}
				if($column=='poslovna_godina_id'){
					$result = $godina_id;
				}
			}
		}else{
			$result = DB::table('roba')->select($column)->where('roba.roba_id',$roba_id)->pluck($column);
		}
		return $result;
		// if($result){
		// 	return $result;
		// }
		// else{
		// 	return '';
		// }
	}

	public static function partner($partner_id) {
		return DB::table('partner')->where('partner_id','!=',-1)->where('partner_id',$partner_id)->first();
	}

	public static function getKupac($id, $column){
		$kupac = DB::table('web_kupac')->where('web_kupac_id', $id)->pluck($column);
		return $kupac;
	}
	public static function kupac($id){
		return DB::table('web_kupac')->where('web_kupac_id', $id)->first();
	}

	public static function createKupac($ime){
		$ime_arr = explode(' ',$ime);
		$data = array(
			'ime' => isset($ime_arr[0]) ? $ime_arr[0] : 'Ime',
			'prezime' => isset($ime_arr[1]) ? $ime_arr[1] : '',
			'email' => isset($ime_arr[2]) ? $ime_arr[2] : '',
			'adresa' => isset($ime_arr[3]) ? $ime_arr[3] : '',
			'mesto' => isset($ime_arr[4]) ? $ime_arr[4] : '',
			'telefon' => isset($ime_arr[5]) ? $ime_arr[5] : '',
			'lozinka' => ''
		);
		DB::table('web_kupac')->insert($data);
		return DB::table('web_kupac')->max('web_kupac_id');
	}

    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }
    	
    public static function selectGroups($grupa_pr_id=null){
        $render = '
        <option value="0" '.(($grupa_pr_id == 0 || is_null($grupa_pr_id) || $grupa_pr_id == 'null') ? 'selected' : '').'>Izaberite grupu</option>
        <option value="-1" '.($grupa_pr_id == -1 ? 'selected' : '').'>Nedefinisana</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

}