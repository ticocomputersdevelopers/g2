<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsSpSoft\DBData;
use IsSpSoft\Article;
use IsSpSoft\Group;
use IsSpSoft\Stock;
use IsSpSoft\Partner;
use IsSpSoft\PartnerCard;
use IsSpSoft\PartnerRabat;
use IsSpSoft\Support;



class AdminIsSpSoft {

    // http://ortopedija-novizivot.selltico.com/auto-import-is/d01292b3e2be9afc657695cd22623ecf
    public static function execute(){

        // try {
            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','rabat','mesto','telefon','pib','broj_maticni'));
            // Partner::query_delete_unexists($resultPartner->body);
            $mappedPartners = Support::getMappedPartners();

            //partner card
            $partnersCards = DBData::partnersCards();
            $resultPartnerCard = PartnerCard::table_body($partnersCards,$mappedPartners);
            PartnerCard::query_insert_update($resultPartnerCard->body);
            PartnerCard::query_delete_unexists($resultPartnerCard->body);

            //articles
            $articles = DBData::articles();

            $groupId = Support::getNewGrupaId('SP SOFT GUPE');
            $groups = Support::groupedGroups($articles);
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body,array('grupa'));
            Group::query_delete_unexists($resultGroup->body);
            Support::updateGroupsParent($groups,$groupId);
            $mapped_groups = Support::getMappedGroups();

            $resultArticle = Article::table_body($articles,$mapped_groups);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','naziv','naziv_web','racunska_cena_nc','racunska_cena_a','racunska_cena_end','mpcena','web_cena','jedinica_mere_id'));
            Article::query_delete_unexists($resultArticle->body);
            $mapped_articles = Support::getMappedArticles();

            $resultStock = Stock::table_body($articles,$mapped_articles);
            Stock::query_insert_update($resultStock->body);

            // $partner_rabat = DBData::partnerGroupRabat();
            // $resultPartnerRabat = PartnerRabat::table_body($partner_rabat,$mappedPartners,$mapped_groups);
            // PartnerRabat::query_insert_update($resultPartnerRabat->body);
            // PartnerRabat::query_delete_unexists($resultPartnerRabat->body);

            // // Support::postUpdate();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        // }catch (Exception $e){
            // AdminB2BIS::saveISLog('false');
            // AdminB2BIS::saveISLogError($e->getMessage());
            // AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
            // return (object) array('success'=>false,'message'=>$e->getMessage());
        // }
    }



}