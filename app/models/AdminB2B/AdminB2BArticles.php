<?php

class AdminB2BArticles {

	public static function fetchAll($criteria, $pagination=null, $sort=null)
	{	
		$level2= array();
		AdminB2BCommon::allGroups($level2,$criteria['grupa_pr_id']); 
		count($level2) ? $level2 : $level2 = array(0);

		$select="SELECT r.roba_id,r.sifra_is, sifra_d, naziv, proizvodjac_id, dobavljac_id, flag_aktivan, flag_prikazi_u_cenovniku, flag_zakljucan, akcija_flag_primeni, tarifna_grupa_id, b2b_max_rabat, web_cena, racunska_cena_nc, r.grupa_pr_id, b2b_akcijski_rabat 
		FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id LEFT JOIN lager l ON l.roba_id = r.roba_id";
		$where = " WHERE r.roba_id <> -1 AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";	

		if(isset($criteria['proizvodjac']) && $criteria['proizvodjac'] != '0'){
			$proizvodjaci = str_replace("-", ", ", $criteria['proizvodjac']);
			$where .= " AND proizvodjac_id IN (". $proizvodjaci .")";
		}
		if(isset($criteria['dobavljac']) && $criteria['dobavljac'] != '0'){
			$dobavljaci = str_replace("-", ", ", $criteria['dobavljac']);
			$where .= " AND dobavljac_id IN (". $dobavljaci .")";
		}

		if(isset($criteria['magacin']) && $criteria['magacin'] != '0'){
			$where .= " AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND l.orgj_id IN (". $criteria['magacin'] .")";
		}else{
			$where .= " AND (l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) OR l.orgj_id IS NULL)";
		}


		if(isset($criteria['filteri'])){
			$filteri = explode("-", $criteria['filteri']);

			if($filteri[0] == "0"){
				$where .= " AND ((l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND kolicina = 0) OR kolicina IS NULL)";
			}
			if($filteri[0] == "1"){
				$where .= " AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND kolicina > 0";
			}
			if($filteri[1] == "0"){
				$where .= " AND akcija_flag_primeni = 0";
			}
			if($filteri[1] == "1"){
				$where .= " AND akcija_flag_primeni = 1";
			}
			if($filteri[2] == "0"){
				$where .= " AND flag_zakljucan = false";
			}
			if($filteri[2] == "1"){
				$where .= " AND flag_zakljucan = true";
			}
			if($filteri[3] == "0"){
				$where .= " AND flag_aktivan <> 1";
			}
			if($filteri[3] == "1"){
				$where .= " AND flag_aktivan = 1";
			}
			if($filteri[4] == "0"){
				$where .= " AND flag_prikazi_u_cenovniku <> 1";
			}
			if($filteri[4] == "1"){
				$where .= " AND flag_prikazi_u_cenovniku = 1";
			}
			if($filteri[5] == "0"){
				$where .= " AND r.roba_id IN (SELECT roba_id FROM dobavljac_cenovnik WHERE roba_id <> -1 AND kolicina = 0)";
			}
			if($filteri[5] == "1"){
				$where .= " AND r.roba_id IN (SELECT roba_id FROM dobavljac_cenovnik WHERE roba_id <> -1 AND kolicina > 0)";
			}
		}

		 $group = "GROUP BY r.roba_id";

		if(isset($sort)){
			$sort = explode("-", $sort);
			// $sort = str_replace('DESC', '', $sort);
			// $sort = str_replace('ASC', '', $sort);
			//$order = " ORDER BY (CASE WHEN (".$sort[0]." ".$sort[1]." ~ '^\d+(.\d+)?$') THEN CAST(".$sort[0]." ".$sort[1]." as int) END) ";
			$order = " ORDER BY ".$sort[0]." ".$sort[1]."";
		}else{
			//$order = " ORDER BY (CASE WHEN (r.sifra_is ~ '^\d+(.\d+)?$') THEN CAST(r.sifra_is as int) END), r.sifra_is ASC ";
			$order = " ORDER BY r.sifra_is ASC ";
		}

		if(isset($criteria['search']) && $criteria['search'] != '0'){

					$where .= " AND naziv ILIKE '%".AdminB2BOptions::search_conver($criteria['search'])."%' OR sifra_d ILIKE '%".AdminB2BOptions::search_conver($criteria['search'])."%' OR sku ILIKE '%".AdminB2BOptions::search_conver($criteria['search'])."%' OR CAST(r.roba_id AS TEXT) LIKE '%".AdminB2BOptions::search_conver($criteria['search'])."%'";

		}

        if(isset($criteria['nabavna'])){
            $nabavna_arr = explode('-',$criteria['nabavna']);
            if(is_numeric($nabavna_arr[0]) && is_numeric($nabavna_arr[1])){
                $where .= " AND racunska_cena_nc BETWEEN ".$nabavna_arr[0]." AND ".$nabavna_arr[1]."";
            }
        }

		if(is_array($pagination)){
			$pagination = " LIMIT ".$pagination['limit']." OFFSET ".$pagination['offset']."";
		}else{
			$pagination = "";
		}

		return DB::select($select.$where.$group.$order.$pagination);
	}

	public static function find($roba_id,$column,$magacin_id=null){
		
		if($column == 'kolicina' || $column == 'orgj_id' || $column == 'poslovna_godina_id'){
			$godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
			$default_magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
			if($magacin_id!=null){
				$default_magacin_id = $magacin_id;
			}
			$result = DB::table('roba')->leftJoin('lager', 'roba.roba_id', '=','lager.roba_id')->select($column)->where(array('roba.roba_id'=>$roba_id,'lager.poslovna_godina_id'=>$godina_id,'lager.orgj_id'=>$default_magacin_id))->pluck($column);
		}else{
			$result = DB::table('roba')->select($column)->where('roba.roba_id',$roba_id)->pluck($column);
		}
		return $result;
		// if($result){
		// 	return $result;
		// }
		// else{
		// 	return '';
		// }
	}

	public static function cena($cena,$valuta_show = false){
        $decimale = 2;

        $valuta = Cache::remember('admin_valuta',20,function(){ return DB::table('valuta')->where('izabran',1)->first(); });

        $kurs=AdminOptions::kurs($valuta->valuta_id);
        $valutaLabel = ' <span> '.strtolower($valuta->valuta_sl).'.</span>';
        if($cena!="0"){            
            $iznos = number_format(round(floatval($cena/$kurs),$decimale), 2, ',', '.');
        }else{
            $iznos = '0.00';
        }

        if($valuta_show == true){
            return $iznos.$valutaLabel;
        }else{
            return $iznos;
        }
    }

	public static function short_title($roba_id){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
		foreach ($query_title as $row){
			if(strlen($row->naziv_web)>70){
			return substr($row->naziv_web,0,67)."...";
			}
			else {
				return $row->naziv_web;
			}
		}
	
	}

	public static function analitika_title($roba_id){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();

		foreach ($query_title as $row){
			if(strlen($row->naziv_web)>70){
			return str_replace('"','inch',substr($row->naziv_web,0,67))."...";
			}
			else {
				return str_replace('"','inch',$row->naziv_web);
			}
		}
	
	}

	
}
