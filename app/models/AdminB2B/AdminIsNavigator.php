<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsNavigator\ApiData;
use IsNavigator\Article;
use IsNavigator\PriceList;
use IsNavigator\Stock;
use IsNavigator\Partner;
use IsNavigator\PartnerCard;
use IsNavigator\Support;



class AdminIsNavigator {

    public static function execute(){
        // http://local.client/auto-import-is/13b80ca7f6c9af1087512c0427179fd5
        try {
            // //partner
            // $partners = ApiData::getRequest('load_accounts_by_node_mapping/KLIJENTI');
            // $resultPartner = Partner::table_body($partners);
            // Partner::query_insert_update($resultPartner->body,array('naziv','adresa'));
            // $mappedPartners = Support::getMappedPartners();

            // //partner card
            // $partnersCards = ApiData::partnersCards();
            // $resultPartner = PartnerCard::table_body($partnersCards,$mappedPartners);
            // PartnerCard::query_insert_update($resultPartner->body);

            // //articles
            // $articles = ApiData::getRequest();
            // // $groups = Support::filteredGroups($articles);

            // $resultArticle = Article::table_body($articles);
            // Article::query_insert_update($resultArticle->body,array('flag_aktivan','naziv','naziv_web','web_opis','proizvodjac_id','jedinica_mere_id','model','garancija','tarifna_grupa_id','barkod','tezinski_faktor','trans_pak','kom_pak'));
            // // Article::query_update_unexists($resultArticle->body);
            // // $mapped_articles = Support::getMappedArticles();

            $priceLists = ApiData::getRequest('load_price_list_items');
            $resultPriceLists = PriceList::table_body($priceLists);
            PriceList::query_insert_update($resultPriceLists->body);


            // $resultStock = Stock::table_body($articles,$mapped_articles);
            // Stock::query_insert_update($resultStock->body);

            // Support::updateGroupVisible();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}