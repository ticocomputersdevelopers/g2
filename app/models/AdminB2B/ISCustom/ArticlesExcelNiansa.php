<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;
use DB;
use All;

class ArticlesExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/roba.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array();
		$codes = array();
		$category_ids = array();
		$manufacturer_ids = array();
		$poreske_stope_ids = array();
		$jedinice_mere_ids = array();

		$roba_id = DB::select("SELECT nextval('roba_roba_id_seq')")[0]->nextval;
		$sifra = DB::select("SELECT nextval('roba_sifra_seq')")[0]->nextval;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $B = $worksheet->getCell('B'.$row)->getValue();
		    $C = $worksheet->getCell('C'.$row)->getValue();
		    $D = $worksheet->getCell('D'.$row)->getValue();
		    $G = $worksheet->getCell('G'.$row)->getValue();
		    $E = $worksheet->getCell('E'.$row)->getValue();
		    $H = $worksheet->getCell('H'.$row)->getValue();
		    $I = $worksheet->getCell('I'.$row)->getValue();
		    $J = $worksheet->getCell('J'.$row)->getValue();
		    $K = $worksheet->getCell('K'.$row)->getValue();

// if(isset($A) && is_numeric($C) && isset($D) && is_numeric($D) && DB::table('grupa_pr')->where('grupa_pr_id',intval($D))->count()>0){
	    	if(isset($A) && is_numeric($C) && isset($D) && is_numeric($D)){	    		
			    // if(!isset($C) || !is_numeric($C)){$C='1';}
		    	if(!isset($E) || !is_numeric($C)){$E='-1';}
				$jm = $G;

				$roba_id++;
				$sifra++;
				$sifra_k++;
				$codes[] = $A;
				$category_ids[] = intval($D);
				$manufacturer_ids[] = intval($E);
				$poreske_stope_ids[] = intval($C);
				$jedinice_mere_ids[] = intval($jm);
				$B = AdminB2BIS::encodeTo1250($B);

				$result_arr[] = "(".strval($roba_id).",".strval($sifra).",'".addslashes(AdminB2BIS::encodeTo1250($A))."','".addslashes($B)."',NULL,NULL,NULL,".$D.",".$C.",".strval($jm).",".$E.",-1,".strval($sifra_k).",NULL,NULL,'".addslashes(substr($B,0,20))."',0,-1,0,0,0,0,9,0,0,0,0,1,1,0,NULL,1,".(isset($H)?$H:'0').",0,0,0,NULL,".(isset($I)?$I:'0').",false,0,(NULL)::integer,'".addslashes($B)."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".(isset($J)?$J:'0').",1,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,'".addslashes(AdminB2BIS::encodeTo1250($K))."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer)";
	    	}

		}
		return (object) array("body"=>implode(",",$result_arr),"codes"=>$codes,"category_ids"=>$category_ids,"manufacturer_ids"=>$manufacturer_ids,"poreske_stope_ids"=>$poreske_stope_ids,"jedinice_mere_ids"=>$jedinice_mere_ids);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra" && $col!="sifra_d" && $col!="web_opis"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}
		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_d=roba_temp.sifra_d ");

		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.sifra_d=roba_temp.sifra_d))");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba), FALSE)");
		DB::statement("SELECT setval('roba_sifra_seq', (SELECT MAX(sifra) FROM roba), FALSE)");
	}

	public static function query_delete($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM roba t WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_d=roba_temp.sifra_d)");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function get_jm_id($jm_string) {
		if($jm_string==trim('kom')){
			$jm = 1;
		}
		elseif($jm_string==trim('pak')){
			$jm = 2;
		}
		elseif($jm_string==trim('m')){
			$jm = 3;
		}
		elseif($jm_string==trim('kgr')){
			$jm = 5;
		}
		else{
			$jm = 4;
		}
		return $jm;
	}
}