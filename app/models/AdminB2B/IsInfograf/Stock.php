<?php
namespace IsInfograf;
use DB;

class Stock {

	public static function table_body($articles,$mapped_articles,$magacin=4){

		$result_arr = array();
		foreach($articles as $article) {
			$id_is = $article->idartikal;
			$roba_id = isset($mapped_articles[strval($id_is)]) ? $mapped_articles[strval($id_is)] : null;

			if(!is_null($roba_id)){
				$kolicina = $article->kol;
				if(intval($article->kol) < 0){
					$kolicina = 0;
				}
				if($kolicina > 0 && $magacin==1){
					$kolicina = 1;
				}

				$rezervisano = 0;
				if($magacin!=1 && !is_null($article->rez) && is_numeric($article->rez) && floatval($article->rez) > 0){
					$rezervisano = $article->rez;
				}

				$result_arr[] = "(".strval($roba_id).",".intval($magacin).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,".strval($rezervisano).",0,0,0,2014,-1,0,0,'".strval($id_is)."')";
			}


		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}


		DB::statement("UPDATE lager t SET kolicina = lager_temp_grouped.kolicina FROM (SELECT MAX(orgj_id) AS orgj_id, SUM(kolicina) AS kolicina, MAX(roba_id) AS roba_id FROM ".$table_temp." GROUP BY roba_id, orgj_id, poslovna_godina_id) AS lager_temp_grouped WHERE t.roba_id=lager_temp_grouped.roba_id AND t.orgj_id=lager_temp_grouped.orgj_id");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id) SELECT MAX(roba_id) AS roba_id, MAX(orgj_id) AS orgj_id, SUM(kolicina) AS kolicina, 2014 FROM ".$table_temp.$where." GROUP BY roba_id, orgj_id, poslovna_godina_id");

	}

}
