<?php
namespace IsRoaming;

use DB;

class PartnerSeller {

    public static function table_body($partners,$mappedSellers,$mappedPartners){
        $result_arr = array();
        $partner_komercijalista_id = DB::select("SELECT MAX(partner_komercijalista_id) + 1 AS max FROM partner_komercijalista")[0]->max;

        foreach($partners as $partner) {
            if(isset($partner->id_s) && (isset($partner->komercijalista) && $partner->komercijalista != '')){
                $sellerArr = explode(' ',$partner->komercijalista);
                $ime = isset($sellerArr[0]) ? $sellerArr[0] : '';
                $prezime = isset($sellerArr[1]) ? $sellerArr[1] : '';

                if($ime != ''){
                    $partner_id = isset($mappedPartners[strval($partner->id_s)]) ? $mappedPartners[strval($partner->id_s)] : null;
                    $imenik_id = isset($mappedSellers[strval(($ime.' '.$prezime))]) ? $mappedSellers[strval(($ime.' '.$prezime))] : null;

                    if(!is_null($partner_id) && !is_null($imenik_id)){
                        $partner_komercijalista_id++;

                        $result_arr[] = "(".strval($partner_komercijalista_id).",".$partner_id.",".$imenik_id.",0,0.00)";

                    }
                }
            }
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_komercijalista'"));
        $table_temp = "(VALUES ".$table_temp_body.") partner_komercijalista_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="partner_komercijalista_id"){
                $updated_columns[] = "".$col." = partner_komercijalista_temp.".$col."";
            }
        }

        //delete
        DB::statement("DELETE FROM partner_komercijalista WHERE (partner_id, imenik_id) NOT IN (SELECT partner_id, imenik_id FROM ".$table_temp.") AND (SELECT kvota FROM imenik WHERE imenik_id=partner_komercijalista.imenik_id) = 32.00");

        //insert
        DB::statement("INSERT INTO partner_komercijalista (SELECT * FROM ".$table_temp." WHERE (partner_id, imenik_id) NOT IN (SELECT partner_id, imenik_id FROM partner_komercijalista))");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}
