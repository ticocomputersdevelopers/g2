<?php
namespace ISWings;
use AdminB2BOptions;

class WingsAPI {

	public static function autorize($username,$password){

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/system.user.log?output=jsonapi",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => '{"aUn": "'.$username.'","aUp": "'.$password.'"}',
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result->data[0]->attributes->token;
	    }		
	}

	public static function grupe($access_token){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/local.cache.vrsta?output=jsonapi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result->data;
	    }		
	}

	public static function magacini($access_token){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/lager.magacin.svi?output=jsonapi",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result->data;
	    }		
	}

	public static function artikli($access_token,$magacin_id=null,$page=1){

		$limit = 1000;
		$offset = ($page-1)*$limit;
		$magacin_id = $magacin_id ? $magacin_id : 0;

		$url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/lager.artikal.list9?nCount=10000&output=jsonapi";

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_TIMEOUT => 300000,
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      if(isset($result->data)){
		      return $result->data;
		  }else{
		  	  return array();
		  }
	    }		
	}

	public static function lager($access_token,$magacin_id=null,$page=1){
		// $url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/local.cache.artikal?dStart=0&dLength=5&dSearch=00&dAtrRet=true&output=jsonapi";

		$limit = 1000;
		$offset = ($page-1)*$limit;
		$magacin_id = $magacin_id ? $magacin_id : 0;

		$url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/local.cache.artikal?dStart=".$offset."&dLength=".strval($limit)."&magacin=".strval($magacin_id)."&output=jsonapi";

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_TIMEOUT => 300000,
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      if(isset($result->data)){
		      return $result->data;
		  }else{
		  	  return array();
		  }
	    }		
	}

	public static function parneri_svi($access_token){
		$url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/local.kupac.svi?output=jsonapi";

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
print_r($result); die;
	      return $result->data;
	    }		
	}

	// public static function kupac_info($access_token){
	// 	$url = "https://portal.wings.rs/api/v1/".AdminB2BOptions::info_sys('wings')->portal."/partner.kupac.info?output=jsonapi";

	// 	$curl = curl_init();
	// 	curl_setopt_array($curl, array(
	// 	  CURLOPT_URL => $url,
	// 	  CURLOPT_RETURNTRANSFER => true,
	// 	  CURLOPT_ENCODING => "",
	// 	  CURLOPT_MAXREDIRS => 10,
	// 	  CURLOPT_TIMEOUT => 30,
	// 	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	// 	  CURLOPT_CUSTOMREQUEST => "GET",
	// 	  CURLOPT_HTTPHEADER => array(
	// 	    "Cookie: PHPSESSID=".$access_token.""
	// 	  ),
	// 	));

	// 	$response = curl_exec($curl);
	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	//     if ($err) {
	//       $error = "cURL Error #:" . $err;
	//       return false;
	//     } else {
	//       $result = json_decode($response);
	//       return $result;
	//     }		
	// }

}