<?php
namespace IsNavigator;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner->id) && $partner->account_type == 1){
		    	$partner_id++;
	            $id_is=$partner->id;
	            $sifra= $partner->code;
	            $naziv = isset($partner->name) ? "'".substr(pg_escape_string($partner->name),0,250)."'" : "NULL";

				$adresa = "NULL";
            	$telefon = "NULL";
            	$mesto = "NULL";
            	$mail = "NULL";
	            if(isset($partner->billing_address) && is_object($partner->billing_address)){
	            	$addressObj = $partner->billing_address;
	            	$adresa=isset($addressObj->address) ? "'".substr(pg_escape_string($addressObj->address),0,250)."'" : "NULL";
	            	$telefon=isset($addressObj->phone) ? "'".pg_escape_string(substr($addressObj->phone,0,250))."'" : "NULL";
	            	$mesto=isset($addressObj->city) ? "'".pg_escape_string(substr($addressObj->city.' '.(isset($addressObj->zip_code) ? $addressObj->zip_code : ''),0,250))."'" : "NULL";
	            	$mail=isset($addressObj->email) ? "'".substr($addressObj->email,0,100)."'" : "NULL";
	            }

	            $pib = "NULL";
	            $broj_maticni = "NULL";
	            $rabat = 0;
	            // $pib=isset($partner->acCode) && is_numeric(intval($partner->acCode)) ? pg_escape_string(substr($partner->acCode,0,100)) : null;
	            // $broj_maticni=isset($partner->acRegNo) ? substr($partner->acRegNo,0,30) : null;
	            // $rabat= isset($partner->anRebate) && is_numeric(floatval($partner->anRebate)) ? $partner->anRebate : 0;

		        $result_arr[] = "(".strval($partner_id).",'".$sifra."',".$naziv.",".$adresa.",".$mesto.",0,".$telefon.",NULL,".strval($pib).",".strval($broj_maticni).",NULL,NULL,NULL,NULL,".strval($rabat).",0,0,".$naziv.",0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,".$mail.",0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::INTEGER,NULL,NULL,0,(NULL)::INTEGER,NULL,(NULL)::DATE)";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}