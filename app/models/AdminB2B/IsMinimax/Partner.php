<?php
namespace IsMinimax;

use DB;
use All;

class Partner {

	public static function table_body($partners){
		$datumKreiranja = date('Y-m-d h:i:s');
		$result_arr = array();
		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner['CustomerId'])){
		    	$partner_id++;
	            
	            $id_is= Support::convert($partner['CustomerId']);
	            $naziv_puni=isset($partner['Name']) ? substr(Support::convert($partner['Name']),0,200) : null;
	            $naziv = $naziv_puni;
	            // $mesto=isset($partner->mesto) ? pg_escape_string(substr($partner->mesto,0,250)) : null;
	            $mesto=Support::convert($partner['City']);
	            $adresa=Support::convert($partner['Address']);
	            $telefon= 'NULL';
	            $pib='NULL';
	            $broj_maticni='NULL';
	            $mail='NULL';
	            $rabat= isset($partner['PartnerDetails']) && isset($partner['PartnerDetails']['RebatePercent']) ? $partner['PartnerDetails']['RebatePercent'] : 0;

		        $result_arr[] = "(".strval($partner_id).",'".strval($id_is)."','".strval($naziv)."','".strval($adresa)."','".strval($mesto)."',0,".$telefon.",NULL,".$pib.",".$broj_maticni.",NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".strval($naziv_puni)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,".$mail.",0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::INTEGER,NULL,NULL,0,(NULL)::INTEGER,NULL,'".$datumKreiranja."'::timestamp)";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);

		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";


		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE partner_temp.id_is NOT IN (SELECT id_is FROM partner WHERE id_is IS NOT NULL AND id_is <> '')");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}