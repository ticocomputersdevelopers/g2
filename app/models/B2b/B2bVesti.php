<?php

class B2bVesti {

	public static function getNews() {
		$news = DB::table('web_vest_b2b')->join('web_vest_b2b_jezik','web_vest_b2b_jezik.web_vest_b2b_id','=','web_vest_b2b.web_vest_b2b_id')->select('*','web_vest_b2b_jezik.sadrzaj as tekst')->where(array('jezik_id'=>1))->orderBy('rbr','asc')->paginate(30);
		return $news;
	}

	public static function shortNewDesc($desc) {
		if(strlen($desc) > 200){
			return strip_tags(substr($desc,0,200).'<span> ...</span>');
		}else{
			return $desc;
		}
	}
}