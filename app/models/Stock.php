<?php

class Stock extends Eloquent
{
    protected $table = 'lager';

    protected $primaryKey = ['roba_id','orgj_id','poslovna_godina_id'];

    public $incrementing = false;
    
    public $timestamps = false;

	protected $hidden = array(
        'roba_id',
        'orgj_id',
        'min_kolicina',
        'max_kolicina',
        'poc_kolicina',
        'kolicina',
        'nabavna_po',
        'nabavna_pr',
        'veleprodajna',
        'maloprodajna',
        'nabavna_po_dev',
        'nabavna_pr_dev',
        'veleprodajna_dev',
        'maloprodajna_dev',
        'valuta_id',
        'rabat_flag',
        'rezervisano',
        'poc_stanje',
        'poc_cena',
        'izdato',
        'poslovna_godina_id',
        'lokacija_magacin_id',
        'k_prosla',
        'razlika',
        'sifra_is'
    	);

    public function getStockroomIdAttribute()
    {
        return isset($this->attributes['orgj_id']) ? intval($this->attributes['orgj_id']) : null;
    }
    public function getArticleIdAttribute()
    {
        return isset($this->attributes['roba_id']) ? intval($this->attributes['roba_id']) : null;
    }
    public function getBusinessYearIdAttribute()
    {
        return isset($this->attributes['poslovna_godina_id']) ? intval($this->attributes['poslovna_godina_id']) : null;
    }
    public function getQuantityAttribute()
    {
        return isset($this->attributes['kolicina']) ? floatval($this->attributes['kolicina']) : 0;
    }
    public function getReservedAttribute()
    {
        return isset($this->attributes['rezervisano']) ? floatval($this->attributes['rezervisano']) : 0;
    }

	protected $appends = array(
        'stockroom_id',
        'article_id',
        'business_year_id',
    	'quantity',
    	'reserved'
    	);

    public function article(){
        return $this->belongsTo('Article','roba_id');
    }

    public function stockroom(){
        return $this->belongsTo('Stockroom','orgj_id');
    }

}