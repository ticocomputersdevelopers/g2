<?php

class IzvestajiController extends Controller {

    public function izvestaj_serviseri(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $status_id = Input::get('status_id');
        //EXTRACT(EPOCH FROM SUM(rn.datum_zavrsetka - rn.datum_prijema) / COUNT(radni_nalog_id))::integer / 60 AS prosecno_vreme
        $query = "SELECT s.ime, s.prezime, COUNT(radni_nalog_id), justify_hours(SUM(rn.datum_zavrsetka - rn.datum_prijema) / COUNT(radni_nalog_id)) AS prosecno_vreme FROM radni_nalog rn LEFT JOIN serviser s ON s.serviser_id = rn.primio_serviser_id";

        $where = false;
        if(!empty($status_id)){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " status_id = ".$status_id."";
        }

        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) > '".$datum_od."'";
        }
        
        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (s.serviser_id) ORDER BY count DESC";

        $data = array(
            array('IME','PREZIME','BROJ REKLAMACIJA', !empty($status_id) && $status_id == 4 ? 'PROSEČNO VREME' : '')
        );
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->ime, $item->prezime, $item->count, !empty($status_id) && $status_id == 4 ? $item->prosecno_vreme : ''
            );
        }
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_serviseri_'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

    public function izvestaj_brendovi(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $query = "SELECT CASE WHEN p.naziv IS NULL THEN 'Bez brenda' ELSE p.naziv END AS naziv, COUNT(radni_nalog_id) FROM radni_nalog rn LEFT JOIN partner p ON p.partner_id = rn.partner_proizvodjac_id";

        $where = false;
        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) > '".$datum_od."'";
        }
        
        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (p.partner_id) ORDER BY count DESC";

        $data = array(
            array('NAZIV BRENDA','BROJ REKLAMACIJA')
        );
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->naziv, $item->count
            );
        }
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_brendovi_'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

    public function izvestaj_grupe(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $query = "SELECT CASE WHEN g.grupa IS NULL THEN 'Bez grupe' ELSE g.grupa END AS naziv, COUNT(radni_nalog_id), (SELECT SUM(iznos) FROM radni_nalog_operacije WHERE radni_nalog_id IN (SELECT unnest(ARRAY_AGG(rn.radni_nalog_id)))) AS iznos FROM radni_nalog rn LEFT JOIN roba r ON r.roba_id = rn.roba_id LEFT JOIN grupa_pr g ON g.grupa_pr_id = r.grupa_pr_id";

        $where = false;
        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) > '".$datum_od."'";
        }

        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (g.grupa_pr_id) ORDER BY count DESC";

        $data = array(
            array('NAZIV GRUPE','BROJ REKLAMACIJA','IZNOS')
        );
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->naziv, $item->count, $item->iznos
            );
        }
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_brendovi_'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

    public function izvestaj_modeli_grupe(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $grupa_pr_id = Input::get('grupa_pr_id');

        $query = "SELECT CASE WHEN g.grupa IS NULL THEN 'Bez grupe' ELSE g.grupa END AS naziv_grupe, CASE WHEN r.naziv_web IS NULL THEN 'Bez modela' ELSE r.naziv_web END AS naziv_modela, COUNT(radni_nalog_id), (SELECT SUM(iznos) FROM radni_nalog_operacije WHERE radni_nalog_id IN (SELECT unnest(ARRAY_AGG(rn.radni_nalog_id)))) AS iznos FROM radni_nalog rn LEFT JOIN roba r ON r.roba_id = rn.roba_id LEFT JOIN grupa_pr g ON g.grupa_pr_id = r.grupa_pr_id";

        $where = false;
        if(!empty($grupa_pr_id)){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $groups=RMA::getLevelGroups($grupa_pr_id);
            $groups[] = (object) array($grupa_pr_id);

            $query .= " r.grupa_pr_id IN (".implode(',',array_map('current',$groups)).")";
        }

        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) > '".$datum_od."'";
        }

        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            if(!$where){
                $query .= " WHERE";
                $where = true;
            }else{
                $query .= " AND";
            }
            $query .= " DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (r.roba_id,g.grupa_pr_id) ORDER BY naziv_grupe, count DESC";

        $data = array(
            array('NAZIV GRUPE','NAZIV MODELA','BROJ REKLAMACIJA','IZNOS')
        );
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->naziv_grupe, $item->naziv_modela, $item->count, $item->iznos
            );
        }
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_brendovi_'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

    public function izvestaj_rezervni_delovi(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $partner_id = Input::get('partner_id');

        $query = "SELECT p.naziv, r.sifra_is, r.naziv_web, sum(rnr.kolicina) as count, (sum(rnr.iznos) / sum(rnr.kolicina)) as prosecna_cena, sum(rnr.iznos) as rnriznos FROM radni_nalog_rezervni_deo rnr LEFT JOIN radni_nalog rn ON rnr.radni_nalog_id = rn.radni_nalog_id LEFT JOIN roba r ON r.roba_id = rnr.roba_id LEFT JOIN partner p ON p.partner_id = rn.partner_id WHERE rn.status_id = 4 AND rnr.iznos > 0";

        $where = false;
        if(!empty($partner_id)){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND rn.partner_id IN (".implode(',',$partner_id).")";
        }

        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND DATE(rn.datum_prijema) > '".$datum_od."'";
        }

        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (r.roba_id, p.partner_id) order by sum(rnr.kolicina) desc, naziv_web asc";

        $data = array(
            array('NAZIV SERVISA','SIFRA','NAZIV MATERIJALA','KOLICINA','CENA','SUMA')
        );

        $ukupno_rnriznos = 0;
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->naziv, $item->sifra_is, $item->naziv_web, $item->count, round($item->prosecna_cena,2), $item->rnriznos 
            );
            $ukupno_rnriznos += $item->rnriznos;
        }
        $data[] = array('','','','','',$ukupno_rnriznos);
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_rezervni_delovi_'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

    public function izvestaj_operacije(){
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $partner_id = Input::get('partner_id');

        $query = "SELECT p.naziv, o.naziv as operacija_naziv, sum(rno.norma_sat), (sum(rno.iznos)/sum(rno.norma_sat)) as cena, sum(rno.iznos) as rnoiznos FROM radni_nalog_operacije rno LEFT JOIN radni_nalog rn ON rno.radni_nalog_id = rn.radni_nalog_id LEFT JOIN operacija_servis os ON os.operacija_servis_id = rno.operacija_servis_id LEFT JOIN operacija o ON o.operacija_id = os.operacija_id LEFT JOIN partner p ON p.partner_id = rn.partner_id WHERE rn.status_id = 4 AND rno.iznos > 0";

        // $where = false;
        if(!empty($partner_id)){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND rn.partner_id IN (".implode(',',$partner_id).")";
        }

        if(!(is_null($datum_od) || $datum_od=='' || $datum_od=='null')){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND DATE(rn.datum_prijema) > '".$datum_od."'";
        }

        if(!(is_null($datum_do) || $datum_do=='' || $datum_do=='null')){
            // if(!$where){
            //     $query .= " WHERE";
            //     $where = true;
            // }else{
            //     $query .= " AND";
            // }
            $query .= " AND DATE(rn.datum_prijema) < '".$datum_do."'";
        }

        $query .= " GROUP BY (o.operacija_id, os.operacija_servis_id, p.partner_id) order by sum(rno.norma_sat) desc, o.naziv asc";

        $data = array(
            array('NAZIV SERVISA','NAZIV OPERACIJE','KOLICINA','CENA','SUMA')
        );

        $ukupno_rnriznos = 0;
        foreach(DB::select($query) as $item){
            $data[] = array(
                $item->naziv, $item->operacija_naziv, $item->sum, round($item->cena,2), $item->rnoiznos 
            );
            $ukupno_rnriznos += $item->rnoiznos;
        }
        $data[] = array('','','','',$ukupno_rnriznos);
        
        $doc = new PHPExcel();
        $doc->setActiveSheetIndex(0);
        $doc->getActiveSheet()->fromArray($data);
         
        $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

        $store_path = 'files/izvestaj_operacije'.date('Y-m-d').'.xls';
        $objWriter->save($store_path);
        return Redirect::to(RmaOptions::base_url().$store_path);        
    }

}