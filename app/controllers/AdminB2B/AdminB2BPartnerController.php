<?php

class AdminB2BPartnerController extends Controller {

	function proizvodjaci($partner_id=null){
        $proizvodjaci = DB::table('proizvodjac')->get();
        if ($partner_id!=-2) {
          // $rabati = AdminB2BProizvodjac::PartnerRabatGrupa();      
		  // $proizvodjaci = !is_null($partner_id) ? AdminB2BProizvodjac::getManufacturers($partner_id) : array();
          foreach ($proizvodjaci as $proizvodjac) {
            $rabat = DB::table('partner_rabat_grupa')->where('grupa_pr_id', '=', -1)->where('partner_id', $partner_id)->where('proizvodjac_id', $proizvodjac->proizvodjac_id)->pluck('rabat');
             if($rabat != null) {
                    $proizvodjac->rabat = $rabat;                
            // var_dump($proizvodjac->rabat);
            // die();
            }else{
                $proizvodjac->rabat = 0;
            }
          }
        }
        $partneri = DB::table('partner')->orderBy('naziv', 'ASC')->get();

        if($partner_id == 'null'){
            $partner_id = -2;
        }

		$data=array(
			"strana" => 'proizvodjaci',
			"title" => 'Rabat partnera i proizvođača',
			"proizvodjaci" =>$proizvodjaci,
            "partneri" => $partneri,
            "partner_id" => $partner_id
		);
		  return View::make('adminb2b/pages/partneri_proizvodjaci', $data);
	}

	function partneri(){

		$data=array(
            "strana"=>'partneri',
            "title"=> 'Rabat partnera',
            "partneri"=> DB::table('partner')->orderBy('naziv', 'ASC')->get()
        );
        return View::make('adminb2b/pages/partneri', $data);
	}

	function ajax() {
		$action = Input::get('action');
		
		if($action == 'rabat_proizvodjac_edit') {
			$proizvodjac_id = Input::get('proizvodjac_id');
			$rabat = Input::get('rabat');
            $partner_id = Input::get('partner_id');
            

            if ($partner_id == -2) {
                 $query = DB::table('proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->update(['rabat' => $rabat]);
            }else if($partner_id != -2){
                $provera = DB::table('partner_rabat_grupa')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->where('grupa_pr_id', -1)->get();
                
               if($provera != null){
    			     $query = DB::table('partner_rabat_grupa')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->where('grupa_pr_id', -1)->update(['rabat' => $rabat]);
                }else{
                    $query = DB::table('partner_rabat_grupa')->insert(array('proizvodjac_id' => $proizvodjac_id, 'partner_id' => $partner_id, 'grupa_pr_id' => -1, 'rabat' => $rabat));
                    
                }
			}
			if($query == true) {
                AdminSupport::saveLog('RABAT_PROIZVODJAC_IZMENI', array('proizvodjac' => $proizvodjac_id, 'partner' => $partner_id));
				return json_encode(1);
			} else {
				return json_encode(0);
			}
		} 

		
	}

	function rabat_partner_edit() {
            $partner_ids = Input::get('partner_ids');
            $rabat = Input::get('rabat');

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET rabat = ". $rabat ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner')->where('partner_id',$partner_id)->pluck('rabat');
            }
            AdminSupport::saveLog('RABAT_PARTNER_IZMENI', $partner_ids);
            return json_encode($response);

	}

	function kategorija_partner_edit() {
		    $partner_ids = Input::get('partner_ids');
            $kat = Input::get('kategorija');

            if($kat != 'Bez kategorije'){
            $kategorija = DB::table('partner_kategorija')->where('naziv',$kat)->pluck('id_kategorije');

            $response = array();
 
            foreach($partner_ids as $partner_id){

                DB::statement("UPDATE partner SET id_kategorije = ". $kategorija ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner_kategorija')->where('id_kategorije',$kategorija)->pluck('naziv');
            }

            AdminSupport::saveLog('KATEGORIJA_PARTNER_IZMENI', $partner_ids);

            return json_encode($response);
        }
        else if($kat == 'Bez kategorije'){

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET id_kategorije = 0 WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = "Bez kategorije";
            }

            AdminSupport::saveLog('KATEGORIJA_PARTNER_IZMENI', $partner_ids);

            return json_encode($response);
        }

	}


	public function partneri_search(){
		$word = trim(Input::get('search'));

		$data=array(
	                "strana"=>'partneri',
	                "title"=> 'Partneri',
	                "partneri" => AdminB2BSupport::searchPartneri($word),
	                "word" => $word
	            );
			return View::make('adminb2b/pages/partneri', $data);
	}

    function rabat_kombinacije($search=null) {
        $page = Input::get('page') ? Input::get('page') : 1;
        $limit = 30;
        $offset = ($page-1)*$limit;

        $query = DB::table('partner_rabat_grupa')
            ->leftJoin('partner', 'partner_rabat_grupa.partner_id', '=', 'partner.partner_id')
            ->leftJoin('grupa_pr', 'partner_rabat_grupa.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->leftJoin('proizvodjac', 'partner_rabat_grupa.proizvodjac_id', '=', 'proizvodjac.proizvodjac_id')
            ->select('partner_rabat_grupa.*', 'partner.naziv as partner_naziv', 'grupa_pr.grupa', 'proizvodjac.naziv as proizvodjac_naziv');

        if(!empty($search)){
            $query = $query
                ->where('partner.partner_id', '!=', -1)
                ->where(function($q) use ($search){
                    $q->where('partner.naziv', 'ILIKE', "%".$search."%")->
                    orWhere('pib', 'ILIKE', "%".$search."%");
                });
        }
        $count_products = count($query->get());
        $kombinacije = $query->orderBy('id')
            ->limit($limit)->offset($offset)->get();

        $data=array(
            "strana" => 'rabat_kombinacije',
            "title" => 'Rabat kombinacije',
            "search" => !is_null($search) ? $search : '',
            "kombinacije" => $kombinacije,
            "count_products" => $count_products,
            "limit" => $limit,
            "proizvodjaci" => AdminB2BSupport::getProizvodjaci(),
            "partneri" => AdminB2BSupport::getDobavljaci()
            
        );
        return View::make('adminb2b/pages/rabat_kombinacije', $data);
    }

	function rabat_kombinacije_edit() {
		$input = Input::get();

		$query = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->count();
		if($query == 0) {
			$insert = DB::table('partner_rabat_grupa')->insert(['partner_id' => $input['partner'], 'grupa_pr_id' => $input['grupa'], 'proizvodjac_id' => $input['proizvodjac'], 'rabat' => $input['rabat']]);
        } else {
            $update = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->update(['rabat' => $input['rabat']]);
        }
        AdminSupport::saveLog('RABAT_KOMBINACIJA_DODAJ', array(DB::table('partner_rabat_grupa')->max('id')));

		return Redirect::to(AdminOptions::base_url().'admin/b2b/rabat_kombinacije');
	}

	function rabat_kombinacije_delete($id) {
        AdminSupport::saveLog('RABAT_KOMBINACIJE_OBRISI', array($id));
        $query = DB::table('partner_rabat_grupa')->where('id', $id)->delete();
        if($query == true) {
		    return json_encode(1);
		} else {
		    return json_encode(0);
		}
	}

	 //partner_kategorije
    function partner_kategorija($id_kategorije=null){
        $data=array(
            "strana"=>'partner_kategorija',
            "title"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : 'Nova kategorija',
            "id_kategorije"=>$id_kategorije,
            "naziv"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : null,
            "rabat"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'rabat') : null,
            "active"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'active') : null 
        );
   
        return View::make('adminb2b/pages/partner_kategorija', $data);
    }

     //izmena kategorije
     function kategorije_edit()
    {   
        $inputs = Input::get();
        // die('test');
        $validator = Validator::make($inputs, array(
            'naziv' => 'required|max:100', 
            'rabat' => 'required|numeric' 
            ));
        if($validator->fails()){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$inputs['id_kategorije'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['id_kategorije'] == 0){                
                $general_data['id_kategorije'] = DB::select("SELECT MAX(id_kategorije) AS max FROM partner_kategorija")[0]->max + 1;
            }

            if(isset($general_data['active'])){
                $general_data['active'] = 1;
            }else{
                $general_data['active'] = 0; 
            }

            if($inputs['id_kategorije'] != 0){
                $rabat = DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->pluck('rabat');
                DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->update($general_data);

                if ($rabat != DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->pluck('rabat')) {
                    AdminSupport::saveLog('B2B_KATEGORIJA_IZMENI_RAB', array($inputs['id_kategorije']));
                }else{
                    AdminSupport::saveLog('B2B_KATEGORIJA_IZMENI', array($inputs['id_kategorije']));
                }
            }else{
                DB::table('partner_kategorija')->insert($general_data);
                AdminSupport::saveLog('B2B_KATEGORIJA_DODAJ', array(DB::table('partner_kategorija')->max('id_kategorije')));
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$general_data['id_kategorije'])->with('message',$message);
        }
    }

    //brisanje kategorije
    function kategorije_delete($id_kategorije)
    {   
        AdminSupport::saveLog('B2B_KATEGORIJA_OBRISI', array($id_kategorije));

        DB::table('partner_kategorija')->where('id_kategorije',$id_kategorije)->delete();
        

        return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija')->with('message','Uspešno ste obrisali sadržaj.');
    } 
    

}