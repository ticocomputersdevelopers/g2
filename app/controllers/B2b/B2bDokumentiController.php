<?php

class B2bDokumentiController extends Controller {

	public function ponude(){
        $params = array(
            'isb2b' => 1
            );
        Input::merge($params);
        return App::make('PonudaController')->ponude();
	}
	public function ponuda($ponuda_id){
        $params = array(
            'isb2b' => 1
            );
        Input::merge($params);
        return App::make('PonudaController')->ponuda($ponuda_id);
	}

    public function ponuda_post(){
        $params = Input::get();
        $params['isb2b'] = 1;
        Input::merge($params);
        return App::make('PonudaController')->ponuda_post();
    }

    public function ponuda_stavka_delete($ponuda_stavka_id){
        $params = array(
            'isb2b' => 1
            );
        Input::merge($params);
        return App::make('PonudaController')->ponuda_stavka_delete($ponuda_stavka_id);
    }

    public function ponuda_delete($ponuda_id){
        $params = array(
            'isb2b' => 1
            );
        Input::merge($params);
        return App::make('PonudaController')->ponuda_delete($ponuda_id);
    }

    public function ponuda_stavka_save(){
        $data = Input::get();
        $responseData = array(
            'success' => false
        );

        if(Session::has('dokumenti_ponuda_id'.Options::server()) && Session::get('dokumenti_ponuda_id'.Options::server()) && !is_null($ponuda=DB::table('ponuda')->where('ponuda_id',Session::get('dokumenti_ponuda_id'.Options::server()))->first())){
            $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
            $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $roba->tarifna_grupa_id)->pluck('porez');

            if(!is_null($ponuda_stavka = DB::table('ponuda_stavka')->where(array('ponuda_id'=>$ponuda->ponuda_id,'roba_id'=>$roba->roba_id))->first())){
                $updateData = array(
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'kolicina' => $ponuda_stavka->kolicina + $data['kolicina'],
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1-$ponuda_stavka->rabat/100) * (1+$ponuda_stavka->pdv/100)
                );
                DB::table('ponuda_stavka')->where('ponuda_stavka_id',$ponuda_stavka->ponuda_stavka_id)->update($updateData);
            }else{
                $rabat = !is_null(B2bPartner::dokumentiUser('admin')) ? B2bArticle::b2bRabatCene($roba->roba_id,DB::table('ponuda')->where('ponuda_id',$ponuda->ponuda_id)->pluck('partner_id'))->ukupan_rabat : 0;
                $insertData = array(
                    'ponuda_id' => $ponuda->ponuda_id,
                    'broj_stavke' => DB::table('ponuda_stavka')->where('ponuda_id',$ponuda->ponuda_id)->max('broj_stavke')+1,
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'kolicina' => $data['kolicina'],
                    'pdv' => $pdv,
                    'rabat' => $rabat,
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1-$rabat/100) * (1+$pdv/100)
                );
                DB::table('ponuda_stavka')->insert($insertData);
                $ponuda_stavka = DB::table('ponuda_stavka')->where(array('ponuda_id'=>$ponuda->ponuda_id,'roba_id'=>$roba->roba_id))->first();
            }

            $iznos = DB::select("select sum(pcena*kolicina) as iznos from ponuda_stavka where ponuda_id=".$ponuda->ponuda_id);
            DB::table('ponuda')->where('ponuda_id',$ponuda->ponuda_id)->update(array('iznos'=>$iznos[0]->iznos));

            $responseData['success'] = true;
        }
        return json_encode($responseData);
    }

}