<?php
use Service\TranslatorService;

class CRMAkcijeController extends Controller {

   //akcija 
    public function akcijaNew($crm_id){
        
        $crm_akcija_tip_id=DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('crm_akcija_tip_id');
        $data=array(
                "strana"=>'crm_akcija',
                "title"=> 'CRM akcija', 
                "crm_id"=>$crm_id,
                "crm_akcija_tip_id"=>$crm_akcija_tip_id,
                "akcije" => DB::table('crm_akcija')->where('crm_id',$crm_id)->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','dsc')->get(),
                "crm_akcija_tip" =>DB::table('crm_akcija_tip')->where('crm_akcija_tip_id', $crm_akcija_tip_id)->pluck('naziv'),
                "crm_akcija_id" => DB::table('crm_akcija')->where('crm_id', $crm_id)->pluck('crm_akcija_id') 
            );

            return View::make('crm/page', $data); 
    }
     public function akcijaCreate($crm_id){
    
       $inputs = Input::get();
     //All::dd($inputs);
        $zavrseno= Input::get('flag_zavrseno');
        $datum_zavrsetka=Input::get('datum_zavrsetka');
        $vreme_kreiranja=Input::get('vreme_kreiranja');
        $vreme_zavrsetka=Input::get('vreme_zavrsetka');
        $query_result=false;
        

        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
                'opis' =>  'max:2000',
                'datum_kreiranja'=> 'required'
               
            );
        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);


        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/crm_akcija/'.$inputs['crm_id'])->withInput()->withErrors($validator_messages);
           var_dump($validator_messages);

        }
        else
        {
            if($inputs['crm_akcija_id']!=0)
            {
                 $query_result=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->update(array('crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'datum'=>$inputs['datum_kreiranja'].''.$vreme_kreiranja,'datum_zavrsetka'=>$datum_zavrsetka.' '.$vreme_zavrsetka));
                 return Redirect::to(AdminOptions::base_url().'crm/crm_home/')->with('message','Uspešno ste izmenili akciju.');
            }
            else{
                if($inputs['datum_zavrsetka']==null)
                {   
                    $zavrseno='0';
                    $query_result=DB::table('crm_akcija')->insert(array('crm_id'=>$inputs['crm_id'],'crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum'=>$inputs['datum_kreiranja'].' '.$vreme_kreiranja,'datum_zavrsetka'=>null));
                }
                elseif($zavrseno!=NULL){
                        $datum_zavrsetka = date('Y-m-d');
                        $zavrseno='1';
                        $query_result=DB::table('crm_akcija')->insert(array('crm_id'=>$inputs['crm_id'],'crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'flag_zavrseno'=>$zavrseno,'datum'=>$inputs['datum_kreiranja'].' '.$vreme_kreiranja,'datum_zavrsetka'=>$datum_zavrsetka.' '.$vreme_zavrsetka));
                    }
                else
                {
                    $query_result=DB::table('crm_akcija')->insert(array('crm_id'=>$inputs['crm_id'],'crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'flag_zavrseno'=>0,'datum'=>$inputs['datum_kreiranja'].' '.$vreme_kreiranja,'datum_zavrsetka'=>$datum_zavrsetka.' '.$vreme_zavrsetka));
                }
            } 
        
            return Redirect::back()->withInput()->with('message','Uspešno ste dodali akciju.');
        }           
    }
	public function akcijaEdit($crm_akcija_id){

    
        $inputs = Input::get();
        ///All::dd($inputs);
        $crm_id=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->pluck('crm_id');
        $stari=json_encode(DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->get());
        $query_result=false;

        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
                'opis' =>  'max:2000'
               
            );
        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_home/crm_akcija/'.$crm_id)->withInput()->withErrors($validator->messages());
        }
        else
        {      
                if($inputs['datum']==null)
                {
                    $crm_id=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->pluck('crm_id');
                 $query_result=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->update(array('crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'datum'=>null,'datum_zavrsetka'=>$inputs['datum_zavrsetka']));
                
                }
                elseif($inputs['datum_zavrsetka'] == null)
                {
                     $crm_id=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->pluck('crm_id');
                 $query_result=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->update(array('crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'datum'=>$inputs['datum'],'datum_zavrsetka'=>null));
                 
                }
                elseif($inputs['datum']==null && $inputs['datum_zavrsetka']==null)
                {
                    $crm_id=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->pluck('crm_id');
                 $query_result=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->update(array('crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'datum'=>null,'datum_zavrsetka'=>null));
                
                }
                
                else
                {
                 $crm_id=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->pluck('crm_id');
                 $query_result=DB::table('crm_akcija')->where('crm_akcija_id',$inputs['crm_akcija_id'])->update(array('crm_akcija_tip_id'=>$inputs['crm_akcija_tip_id'],'opis'=>$inputs['opis'],'datum'=>$inputs['datum'],'datum_zavrsetka'=>$inputs['datum_zavrsetka']));
                 
                }
               if($query_result==true){
                    $akcija= 'Izmena AKCIJE sa id  '.$crm_akcija_id;
                    $date=date('Y-m-d H:m:s');
                    $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
                    $novi=json_encode($inputs); 
                    DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'nova_vrednost'=>$novi,'imenik_id'=>$imenik_id));
                }
                 return Redirect::to(AdminOptions::base_url().'crm/crm_home/crm_akcija/' .$crm_id )->with('message','Uspešno ste izmenili akciju.');
        }
    }


	public function akcijaDelete($crm_akcija_id)
    {   
        $akcija= 'Brisanje AKCIJE sa id  '.$crm_akcija_id;
        $date=date('Y-m-d H:m:s');
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        $stari=json_encode(DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->get());
        DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
        DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->delete();
       return Redirect::back()->with('message','Uspešno ste obrisali akciju.');
    }

    public function akcija_zavrsena($crm_akcija_id){
        $query_result=false;
         $datum_zavrsetka = date('Y-m-d');
         if(empty(Input::get('datum_zavrsetka'))){
            DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->update(array('flag_zavrseno'=>1,'datum_zavrsetka'=> $datum_zavrsetka));
            $query_result=true;
         }
         else{
            DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->update(array('flag_zavrseno'=>1,'datum_zavrsetka'=>Input::get('datum_zavrsetka')));
            $query_result=true;
         }
         if($query_result==true){
            $akcija= 'Zavrsena akcija sa id  '.$crm_akcija_id;
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->get());
            DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
            
         }
        return Redirect::back()->with('message','Uspešno ste završili akciju.');
    }
    public function akcija_vrati($crm_akcija_id){
        //var_dump($crm_akcija_id);die;
         $datum = date('Y-m-d');
         DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->update(array('flag_zavrseno'=>0,'datum'=> $datum));
            $akcija= 'Ponovo aktivirana akcija sa id  '.$crm_akcija_id;
            $date=date('Y-m-d H:m:s');
            $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
            $stari=json_encode(DB::table('crm_akcija')->where('crm_akcija_id',$crm_akcija_id)->get());
            DB::table('crm_log')->insert(array('datum_izmene'=>$date, 'akcija'=>$akcija,'stara_vrednost'=>$stari,'imenik_id'=>$imenik_id));
         return Redirect::back()->with('message','Uspešno ste vratili akciju.');
    }


}