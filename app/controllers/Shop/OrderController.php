﻿﻿<?php
use Service\MSUApi;
use Service\Drip;
use Service\Mailer;

class OrderController extends BaseController{
    public function order_create(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();
        $stavka=DB::select("SELECT COUNT(*) FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = ".Cart::korpa_id()."")[0]->count;
        if(intval($stavka)==0){
        return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'));
        }
     
        if(!Session::has('b2c_kupac')){
            $validator_arr = array(
                'flag_vrsta_kupca' => 'required|in:0,1',
                'email' => 'required|email|unique:web_kupac,email,NULL,id,status_registracije,1',
                'telefon' => 'required|regex:'.Support::regex().'|between:3,15',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,50'
                );

            $translate_mess =  Language::validator_messages();
            
            if($data['flag_vrsta_kupca'] == 0){
                unset($data['naziv']);
                unset($data['pib']);
                unset($data['maticni_br']);
                $validator_arr['ime'] = 'required|regex:'.Support::regex().'|between:3,20';
                $validator_arr['prezime'] = 'required|regex:'.Support::regex().'|between:3,20';
            }
            if($data['flag_vrsta_kupca'] == 1){
                unset($data['ime']);
                unset($data['prezime']);
                $validator_arr['naziv'] = 'required|regex:'.Support::regex().'|between:3,200';
                $validator_arr['pib'] = 'required|digits_between:9,9|numeric';
                $translate_mess['digits_between'] = 'Broj cifara mora biti 9!';
                $translate_mess['numeric'] = 'Polje sme da sadrzi samo brojeve!';
            }


            $validator = Validator::make($data,$validator_arr,$translate_mess);
            if($validator->fails() || !Captcha::check()){
                if(!Captcha::check()){
                    $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
                }
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa').'#cart_form_scroll')->withInput()->withErrors($validator->messages());
            }
            else {
                $order_data = array(
                    'web_nacin_isporuke_id' => $data['web_nacin_isporuke_id'],
                    'web_nacin_placanja_id' => $data['web_nacin_placanja_id'],
                    'napomena' => $data['napomena'],
                    'stornirano' => $data['web_nacin_placanja_id'] == 3 ? 1 : 0
                );

                $data['kod'] = All::userCodeGenerate();
                $data['lozinka'] = '';
                $data['status_registracije'] = 0;
                $data['flag_potvrda'] = 0;

                unset($data['web_nacin_isporuke_id']);
                unset($data['web_nacin_placanja_id']);
                unset($data['napomena']);
                unset($data['captcha-string']);


                DB::table('web_kupac')->insert($data);
                $kupac_id = DB::select("SELECT currval('web_kupac_web_kupac_id_seq')")[0]->currval;
                $order_data['web_kupac_id'] = $kupac_id;
                Session::put('b2c_kupac_temp',$kupac_id);

                //drip
                if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
                    $drip = new Drip();
                    $checkCreateSubscriber = true;
                    if(is_null($drip->getSubscriber($data['email']))){
                        $dbCustomer = DB::table('web_kupac')->where('web_kupac_id',$kupac_id)->first();
                        $checkCreateSubscriber = $drip->addOrUpdateSubscriber($dbCustomer);
                    }
                    if($checkCreateSubscriber){
                        $dbcart = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',Cart::korpa_id())->first();
                        $dbcartArr = (array) $dbcart;
                        $dbcartArr['web_kupac_id'] = $kupac_id;
                        $drip->addOrUpdateCart(((object) $dbcartArr),"created");    
                    }       
                }
                
                Order::cart_to_order($order_data);
                $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;

                if(Config::get('app.livemode')){
                    $response = null;
                    if(Options::info_sys('logik')){
                        $response = OrderIS::createOrderLogik($order_data);
                    }elseif(Options::info_sys('sbcs')){
                        $response = OrderIS::createOrderSbcs($order_data);
                    }elseif(Options::info_sys('wings')){
                        $response = OrderIS::createOrderWings($order_data);
                    }

                    if(!is_null($response) && !$response->success){
                        $num_nar = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('broj_dokumenta');
                        $mailFrom=Options::company_email();
                        $mailTo=$mailFrom;
                        $subject='Greska pri slanju porudzbine u IS-u ('.$num_nar.')';
                        $body='<h3>Došlo je do greske pri slanju narudžbine u vašem IS-u. Proverite vašu narudžbinu '.$num_nar.' u admin panelu.</h3>';
                        Mailer::send($mailFrom,$mailTo,$subject,$body);
                    }
                }

                if($order_data['web_nacin_placanja_id'] == 3){
                    if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data'))){
                        return Redirect::to(Options::base_url().'intesa/'.$web_b2c_narudzbina_id);
                    }else{
                        $payload = MSUApi::getPayload($web_b2c_narudzbina_id);
                        if(count($payload) > 0){
                            $session = MSUApi::getSessionToken($payload);
                            if(!is_null($session)){
                                // return Redirect::to('https://entegrasyon.asseco-see.com.tr/chipcard/pay3d/'.$session);
                                return Redirect::to(Options::base_url().'msu/'.$web_b2c_narudzbina_id.'/'.$session);
                            }
                        }
                        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('stornirano'=>1));
                        return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške u sistemu platnih kartica').'.');
                    }
                }else{
                    return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
                }
            }
        }else{
            //BODOVI I VAUCERI
            $bodovi = 0;
            if(isset($data['bodovi']) && !empty($data['bodovi'])){
                if(strlen($data['bodovi']) < 10 && is_numeric($data['bodovi'])){
                    $bodovi = intval($data['bodovi']);
                }else{
                    return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->withInput()->with('bodovi_error',Language::trans('Vrednost bodova je neodgovarajuća.'));
                }
                unset($data['bodovi']);
            }
            $vaucerCode = null;
            if(isset($data['vaucer_code']) && !empty($data['vaucer_code'])){
                if(strlen($data['vaucer_code']) < 300){
                    $vaucerCode = $data['vaucer_code'];
                }else{
                    return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->withInput()->with('vaucer_error',Language::trans('Kod vaučera je neodgovarajući.'));
                }
                unset($data['vaucer_code']);
            }

            //BODOVI
            if(Options::web_options(314) == 1){
                $kupacBodovi = WebKupac::bodovi();
                if($bodovi == 0){
                    if(is_null($vaucerCode)){           
                        $bodoviOstvareniOpseg = Cart::bodoviOstvareniOpseg();
                        $bodoviOstvareniBodoviKorpa = Cart::bodoviOstvareniBodoviKorpa($bodoviOstvareniOpseg);
                        if($kupacBodovi == 0){
                            DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->update(array('bodovi'=>$bodoviOstvareniBodoviKorpa));
                        }else{
                            DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->update(array('bodovi'=>DB::raw('bodovi + '.$bodoviOstvareniBodoviKorpa)));
                        }
                    }
                }else{
                    $bodoviPopustOpseg = Cart::bodoviPopustOpseg();
                    $bodoviPopustBodoviKorpa = Cart::bodoviPopustBodoviKorpa($bodoviPopustOpseg);

                    if($bodovi > $kupacBodovi || $bodovi > $bodoviPopustBodoviKorpa){
                        return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->withInput()->with('bodovi_error',Language::trans('Vrednost bodova je neodgovarajuća.'));
                    }else{
                        DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->update(array('bodovi'=>DB::raw('bodovi - '.$bodovi)));
                        $data['popust'] = $bodovi*Options::web_options(317);
                    }
                }
            }
            // VAUCERI
            if(Options::web_options(318) == 1 && $bodovi == 0 && !is_null($vaucerCode)){
                $vauceri = DB::select("select * from vaucer where datum_od < '".date("Y-m-d")."' and datum_do >= '".date("Y-m-d")."' and realizovan = 0 and vaucer_broj = '".$vaucerCode."'");
                if(count($vauceri)==1){
                    $vaucer = $vauceri[0];
                    $vauceriPopustCenaKorpa = Cart::vauceriPopustCenaKorpa();
                    $data['popust'] = ($vauceriPopustCenaKorpa > $vaucer->iznos) ? $vaucer->iznos : $vauceriPopustCenaKorpa;
                    DB::table('vaucer')->where('vaucer_id',$vaucer->vaucer_id)->update(array('realizovan'=>1,'web_kupac_id'=>Session::get('b2c_kupac')));
                }else{
                    return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->withInput()->with('vaucer_error',Language::trans('Kod vaučera je neodgovarajući.'));
                }
            }



            if(!Captcha::check()){
                $translate_mess =  Language::validator_messages();
                $validator = Validator::make($data,array());
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->withInput()->withErrors($validator->messages());
            }

            $data['web_kupac_id'] = Session::get('b2c_kupac');
            $data['stornirano'] = $data['web_nacin_placanja_id'] == 3 ? 1 : 0;
            Order::cart_to_order($data);
            $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;

            if(Config::get('app.livemode')){
                $response = null;
                if(Options::info_sys('logik')){
                    $response = OrderIS::createOrderLogik($data);
                }elseif(Options::info_sys('sbcs')){
                    $response = OrderIS::createOrderSbcs($data);
                }elseif(Options::info_sys('wings')){
                    $response = OrderIS::createOrderWings($data);
                }

                if(!is_null($response) && !$response->success){
                    $num_nar = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('broj_dokumenta');
                    $mailFrom=Options::company_email();
                    $mailTo=$mailFrom;
                    $subject='Greska pri slanju porudzbine u IS-u ('.$num_nar.')';
                    $body='<h3>Došlo je do greske pri slanju narudžbine u vašem IS-u. Proverite vašu narudžbinu '.$num_nar.' u admin panelu.</h3>';
                    Mailer::send($mailFrom,$mailTo,$subject,$body);
                }
            }

            if($data['web_nacin_placanja_id'] == 3){
                if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data'))){
                    return Redirect::to(Options::base_url().'intesa/'.$web_b2c_narudzbina_id);
                }else{
                    $payload = MSUApi::getPayload($web_b2c_narudzbina_id);
                    if(count($payload) > 0){
                        $session = MSUApi::getSessionToken($payload);
                        if(!is_null($session)){
                            // return Redirect::to('https://entegrasyon.asseco-see.com.tr/chipcard/pay3d/'.$session);
                            return Redirect::to(Options::base_url().'msu/'.$web_b2c_narudzbina_id.'/'.$session);
                        }
                    }
                    DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('stornirano'=>1));
                    return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške u sistemu platnih kartica').'.');
                }
            }else{
                return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
            }          
        }    
    }
    public function narudzbina(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);
        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();

        if(!Session::has('b2c_korpa') || is_null($web_b2c_narudzbina) || (!is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->stornirano==1)){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');
        $subject=Language::trans('Porudžbina')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        $kupac = DB::table('web_kupac')->where('web_kupac_id',DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id'))->first();
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'oreder',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> $web_b2c_narudzbina->web_nacin_placanja_id==3 ? json_decode($web_b2c_narudzbina->result_code) : null,
            "kupac"=>$kupac,
            "message" => !Session::has('b2c_kupac') ? true : false
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);
        All::send_email_to_admin($body,$kupac->email,$name,$subject);
        if(!Session::has('b2c_kupac')){
            $kupac_id = Session::get('b2c_kupac_temp');
            Session::put('b2c_kupac',$kupac_id);
            Session::forget('b2c_kupac_temp');
            Session::forget('b2c_kupac');
        }
        $articlesDetails = [];
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $stavka){
            $articlesDetails[] = Product::gtArticleDetails($stavka->roba_id,$stavka->kolicina);
        }
        $data['articles_details'] = $articlesDetails;
        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

    public function select_narudzbina_mesto(){
        $lang = Language::multi() ? Request::segment(1) : null;
        $narudzbina_opstina_id = Input::get('narudzbina_opstina_id');

        $narudzbina_opstina = DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',$narudzbina_opstina_id)->first();
        if(!is_null($narudzbina_opstina)){
            $narudzbina_mesta = DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',$narudzbina_opstina->code)->orderBy('naziv','asc')->get();
            if(count($narudzbina_mesta) > 0){
                $narudzbina_mesto = $narudzbina_mesta[0];
                $narudzbina_ulice = DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',$narudzbina_mesto->code)->orderBy('naziv','asc')->get();
                $select_mesta = View::make('shop/ajax/select-narudzbina-mesto',array('narudzbina_mesta'=>$narudzbina_mesta))->render();
                $select_ulice = View::make('shop/ajax/select-narudzbina-ulica',array('narudzbina_ulice'=>$narudzbina_ulice))->render();

                echo $select_mesta."===DEVIDER===".$select_ulice;      
            }
        }
        echo '===DEVIDER===';
    }

    public function select_narudzbina_ulica(){
        $lang = Language::multi() ? Request::segment(1) : null;
        $narudzbina_mesto_id = Input::get('narudzbina_mesto_id');

        $narudzbina_mesto = DB::table('narudzbina_mesto')->where('narudzbina_mesto_id',$narudzbina_mesto_id)->first();

            if(!is_null($narudzbina_mesto)){
                $narudzbina_ulice = DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',$narudzbina_mesto->code)->orderBy('naziv','asc')->get();
                $select_ulice = View::make('shop/ajax/select-narudzbina-ulica',array('narudzbina_ulice'=>$narudzbina_ulice))->render();

                
                    echo $select_ulice;                
            }
        echo '';
    }
    public function stampanje_narudzbenice(){ 
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
       
        $web_b2c_narudzbina_id = Request::segment(2+$offset);
        $web_kupac_kod = Request::segment(3+$offset);

        // $subject=Language::trans('Porudžbina')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        $kupac = DB::table('web_kupac')->where(array('web_kupac_id'=>DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id'),'kod'=>$web_kupac_kod))->first();
        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();

        if(is_null($kupac) || is_null($web_b2c_narudzbina)) { 
            return Redirect::to(Options::base_url());
        }
        
        $data=array(
            "strana"=>'order',
            "org_strana"=>'oreder',
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> $web_b2c_narudzbina->web_nacin_placanja_id==3 ? json_decode($web_b2c_narudzbina->result_code) : null,
            "kupac"=>$kupac,
            "message" => !Session::has('b2c_kupac') ? true : false
            );


        $pdf = App::make('dompdf');
        
        $pdf->loadView('shop.themes.bsmodern.partials.order_details_print', $data);
        try {
            return $pdf->stream();
        } catch (Exception $e) {
            return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
        }
    }

}
