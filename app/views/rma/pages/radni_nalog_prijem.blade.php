@extends('rma.templates.main')
@section('content')
<section id="main-content">

	<div class="row">
		@include('rma/partials/radni_nalog_tabs')
		<div class="flat-box">
			<div class="columns medium-12">
				<h1>{{ $radni_nalog->broj_naloga }} ({{RMA::getStatusTitle($radni_nalog->status_id)}})</h1>
			</div>
		</div>
		<form class="flat-box" method="POST" action="{{RmaOptions::base_url()}}rma/radni-nalog-prijem-post"> 
			<div class="row">
				<div class="field-group columns medium-5">
					<div class="row"> 
						<div class="columns medium-8"> 
						<label for="">Servis</label>
							<select name="partner_id" id="JSPartner" class="{{ $errors->first('partner_id') ? 'error' : '' }}">
								{{ RMA::servisSelect(Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id) }}
							</select>
						</div>
						<div class="columns medium-4"> 
							<label for="">&nbsp;</label>
							@if(!is_null(RmaOptions::user('admin')))
			                <a href="#" id="JSPartnerDetail" class="btn btn-primary btn-small"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                <a href="{{ RmaOptions::base_url() }}admin/kupci_partneri/partner/0" class="btn btn-primary btn-small" target="_blank">Kreiraj partnera</a>
			                @endif
			            </div>
	                </div>
 
					<div id="JSPartnerAjaxContent" class="rma-user-info">
						{{ RMA::partnerDetails(Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id) }}
					</div> 
	            </div>

				<div class="field-group columns medium-5">
					<div class="row">  
						<div class="columns medium-8">
							<div class="columns medium-12">
								<label for="">Kupac</label>
								@if($radni_nalog->radni_nalog_id == 0)
								<input type="radio" name="vrsta_kupca" class="JSVrstaKupca" value="b2c" {{ ((Input::old('vrsta_kupca') ? Input::old('vrsta_kupca') : $radni_nalog->vrsta_kupca) == 'b2c') ? 'checked' : '' }}>B2C
								<input type="radio" name="vrsta_kupca" class="JSVrstaKupca" value="b2b" {{ ((Input::old('vrsta_kupca') ? Input::old('vrsta_kupca') : $radni_nalog->vrsta_kupca) == 'b2b') ? 'checked' : '' }}>B2B
								@else
								<input type="hidden" name="vrsta_kupca" value="{{ $radni_nalog->vrsta_kupca }}">
								@endif

								<input type="hidden" name="kupac_id" id="JSKupacId" value="{{ (Input::old('kupac_id') ? Input::old('kupac_id') : (!is_null($radni_nalog->kupac_id) ? $radni_nalog->kupac_id : $radni_nalog->b2b_partner_id)) }}">

								<input type="text" name="kupac_ime" id="JSKupacIme" value="{{ ((Input::old('vrsta_kupca') ? Input::old('vrsta_kupca') : $radni_nalog->vrsta_kupca) == 'b2c') ? (!is_null($kupac = RMA::kupac((Input::old('kupac_id') ? Input::old('kupac_id') : $radni_nalog->kupac_id))) ? $kupac->ime.' '.$kupac->prezime : '') : (!is_null($kupac = RMA::partner((Input::old('kupac_id') ? Input::old('kupac_id') : $radni_nalog->b2b_partner_id))) ? $kupac->naziv : '') }}" class="{{ $errors->first('kupac_ime') ? 'error' : '' }}" {{ ($radni_nalog->radni_nalog_id > 0) ? 'disabled="disabled"' : '' }} autocomplete="off" placeholder="Ime Prezime">
								<div id="JSKupacSearchContent" class="rma-custom-select"></div>
							</div>

							<div class="columns medium-12" id="JSKupacPolja" hidden>
								<div class="columns medium-8">
									<input type="text" name="kupac_adresa" value="{{ Input::old('kupac_adresa') }}" class="{{ $errors->first('kupac_adresa') ? 'error' : '' }}" autocomplete="off" placeholder="Adresa">
								</div>
								<div class="columns medium-4">
									<input type="text" name="kupac_mesto" value="{{ Input::old('kupac_mesto') }}" class="{{ $errors->first('kupac_mesto') ? 'error' : '' }}" autocomplete="off" placeholder="Mesto">
								</div>
								<div class="columns medium-6">
									<input type="text" name="kupac_telefon" value="{{ Input::old('kupac_telefon') }}" class="{{ $errors->first('kupac_telefon') ? 'error' : '' }}" autocomplete="off" placeholder="Telefon">
								</div>
								<div class="columns medium-6">
									<input type="text" name="kupac_email" value="{{ Input::old('kupac_email') }}" class="{{ $errors->first('kupac_email') ? 'error' : '' }}" autocomplete="off" placeholder="E-mail">
								</div>
							</div>
						</div>
						<div class="columns medium-4">
							<label for="">&nbsp;</label>
							@if(!is_null(RmaOptions::user('admin')))
			                <a href="#" id="JSKupacDetail" class="btn btn-primary btn-small"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                @endif
			                <a href="#" id="JSKupacDataCreate" class="btn btn-primary btn-small">Kreiraj kupca</a>
			            </div>
	                </div> 

					<div id="JSKupacAjaxContent" class="rma-user-info">
						{{ ((Input::old('vrsta_kupca') ? Input::old('vrsta_kupca') : $radni_nalog->vrsta_kupca) == 'b2c') ? RMA::kupacDetails($radni_nalog->kupac_id) : RMA::partnerDetails($radni_nalog->b2b_partner_id) }}
					</div> 
	            </div>

	            <div class="field-group columns medium-2 text-right">
					<label>&nbsp;</label>
					<a class="btn btn-primary btn-small" href="{{ RmaOptions::base_url() }}/rma/radni-nalog-prijem-dokument/{{ $radni_nalog->radni_nalog_id }}" target="_blank">Prijemnica</a> 
            	</div>
	        </div>
	        
	        <div class="row">
				<div class="field-group columns medium-3">
					<label for="">Primio</label>
					<select name="primio_serviser_id" class="{{ $errors->first('primio_serviser_id') ? 'error' : '' }}">
						{{ RMA::serviseriSelect((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),(!is_null(Input::old('primio_serviser_id')) ? Input::old('primio_serviser_id') : $radni_nalog->primio_serviser_id)) }}
					</select>
				</div>
				<div class="field-group columns medium-3">
					<label for="">Uređaj</label>

					<div class="custom-select-arrow">
						<input name="uredjaj" id="JSUredjajPrijem" class="{{ $errors->first('uredjaj') ? 'error' : '' }}" type="text" value="{{ htmlentities(Input::old('uredjaj') ? Input::old('uredjaj') : $radni_nalog->uredjaj) }}" autocomplete="off">
						<div id="JSUredjajSearchContent" class="rma-custom-select"></div>
					</div>
				</div> 
				<div class="field-group columns medium-2">
					<label for="">Proizvodjač uređaja</label>
					<input name="proizvodjac" class="{{ $errors->first('proizvodjac') ? 'error' : '' }}" type="text" value="{{ Input::old('proizvodjac') ? Input::old('proizvodjac') : $radni_nalog->proizvodjac }}" >
				</div> 
				<div class="field-group columns medium-2">
					<label for="">Serijski broj</label>
					<input name="serijski_broj" class="{{ $errors->first('serijski_broj') ? 'error' : '' }}" type="text" value="{{ Input::old('serijski_broj') ? Input::old('serijski_broj') : $radni_nalog->serijski_broj }}" >
				</div> 
 
				<div class="field-group columns medium-2">
					<label for="">Broj fiskalnog računa</label>
					<input name="broj_fiskalnog_racuna" class="{{ $errors->first('broj_fiskalnog_racuna') ? 'error' : '' }}" type="text" value="{{ Input::old('broj_fiskalnog_racuna') ? Input::old('broj_fiskalnog_racuna') : $radni_nalog->broj_fiskalnog_racuna }}" >
				</div>
			</div>

			<div class="row">   
				<div class="field-group columns medium-2">
					<label for="">Datum prijave reklamacije</label>
					<input name="datum_prijema" class="datum-val has-tooltip {{ $errors->first('datum_prijema') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_prijema') ? Input::old('datum_prijema') : $radni_nalog->datum_prijema }}" autocomplete="off">
				</div> 
				<div class="field-group columns medium-2">
					<label for="">Datum izlaska na servis</label>
					<input name="datum_servisiranja" class="datum-val has-tooltip {{ $errors->first('datum_servisiranja') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_servisiranja') ? Input::old('datum_servisiranja') : $radni_nalog->datum_servisiranja }}" autocomplete="off">
				</div> 

				<div class="field-group columns medium-4">
					<label for="">Naziv maloprodaje</label>
					<input name="maloprodaja" class="{{ $errors->first('maloprodaja') ? 'error' : '' }}" type="text" value="{{ Input::old('maloprodaja') ? Input::old('maloprodaja') : $radni_nalog->maloprodaja	 }}" >
				</div>
				<div class="field-group columns medium-2">
					<label for="">Datum kupovine</label>
					<input name="datum_kupovine" class="datum-val has-tooltip {{ $errors->first('datum_kupovine') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_kupovine') ? Input::old('datum_kupovine') : $radni_nalog->datum_kupovine }}" autocomplete="off">
				</div>
			</div>
			<div class="row">   
				<div class="field-group columns medium-6">
					<label for="">Opis kvara</label>
					<textarea name="opis_kvara" rows="2" class="{{ $errors->first('opis_kvara') ? 'error' : '' }}">{{ Input::old('opis_kvara') ? Input::old('opis_kvara') : $radni_nalog->opis_kvara }}</textarea>
				</div> 
				<div class="field-group columns medium-6">
					<label for="">Napomena</label>
					<textarea name="napomena" rows="2" class="{{ $errors->first('napomena') ? 'error' : '' }}">{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $radni_nalog->napomena }}</textarea>
				</div>
			</div>

			<input type="hidden" name="check_serijski_broj" value="{{ !is_null(Input::old('check_serijski_broj')) ? Input::old('check_serijski_broj') : 1 }}">
			<input type="hidden" name="radni_nalog_id" value="{{ $radni_nalog->radni_nalog_id }}">

			<div class="columns medium-12 large-12 text-center">
				<div class="btn-container"> 
					<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
				</div>
			</div>
		</form>
	</div>
	@if(count(Input::old()) > 0)
	<script type="text/javascript">
		@if((!is_null(Input::old('check_serijski_broj')) ? Input::old('check_serijski_broj') : 1) == 1)
	  		var check = confirm("Postoji radni nalog sa tim serijskim brojem!\nDa li želite da nastavite?");
	  		if(check == true){
	  			document.querySelector('input[name="check_serijski_broj"]').value = '0';
	  			document.querySelector("form").submit();
	  		}
		@endif
	</script>
	@endif
</section>
@endsection