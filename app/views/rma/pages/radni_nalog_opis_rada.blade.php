@extends('rma.templates.main')
@section('content')
<section id="main-content" class="rma-work-descript">
	<div class="row">
		@include('rma/partials/radni_nalog_tabs')
		<div class="flat-box">
			<div class="columns medium-12">
				<h1>{{ $radni_nalog->broj_naloga }} ({{RMA::getStatusTitle($radni_nalog->status_id)}})</h1>
			</div>
		</div>
	</div>

 	<div class="row">
 		<div class="flat-box">
			<h3 class="title-med">Operacije</h3>
			<div class="columns medium-9">
				<table>
					<tr>
						<th>Naziv operacije</th>
						<th>Serviser</th>
						<th>Iznos</th>
						<td></td>
					</tr>
					@foreach($radni_nalog_operacije as $row)
					<tr>
						<td>{{ $row->opis_operacije }}</td>
						<td>{{ RMA::serviser($row->serviser_id)->ime.' '.RMA::serviser($row->serviser_id)->prezime }}</td>
						<td>{{ $row->iznos }}</td>
						<td class="table-col-icons text-right">
							<a class="edit icon JSEditRNOperacija" data-radni_nalog_id="{{ $row->radni_nalog_id }}" data-radni_nalog_operacije_id="{{ $row->radni_nalog_operacije_id }}" href="#"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Izmeni</a>
							<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog-operacija/{{ $row->radni_nalog_operacije_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;Obriši</a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>

			<div class="columns medium-3">
				<a class="btn btn-primary btn-small edit icon JSEditRNOperacija" data-radni_nalog_id="{{ $radni_nalog->radni_nalog_id }}" data-radni_nalog_operacije_id="0" href="#">Dodaj</a>
				<br>
				UKUPNO: {{ RMA::format_cene($radni_nalog_operacije_ukupno) }}
			</div>
		</div>
	</div>

 	<div class="row">
 		<div class="flat-box">
			<h3 class="title-med">Utrošeni rezervni delovi</h3>
			<div class="columns medium-9">
				<table>
					<tr>
						<th>Rezervni deo</th>
						<th>Kom</th>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<th>Cena</th>
						<th>Ukupno</th>
						@endif
						<td></td>
					</tr>
					@foreach($radni_nalog_rezervni_delovi as $row)
					<tr>
						<td>{{ RMA::find($row->roba_id,'naziv_web') }}</td>
						<td>{{ $row->kolicina }}</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td>{{ $row->cena }}</td>
						<td>{{ $row->iznos }}</td>
						@endif
						<td class="table-col-icons text-right">
							<a class="edit icon JSEditRNRezervniDeo" data-radni_nalog_id="{{ $row->radni_nalog_id }}" data-radni_nalog_rezervni_deo_id="{{ $row->radni_nalog_rezervni_deo_id }}" href="#"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Izmeni</a>
							<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog-rezervni-deo/{{ $row->radni_nalog_rezervni_deo_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;Obriši</a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>

			<div class="columns medium-3">
				<a class="btn btn-primary btn-small edit icon JSEditRNRezervniDeo" data-radni_nalog_id="{{ $radni_nalog->radni_nalog_id }}" data-radni_nalog_rezervni_deo_id="0" href="#">Dodaj</a>
				<br>
				@if(RmaOptions::gnrl_options(3043) == 1)
				UKUPNO: {{ RMA::format_cene($radni_nalog_rezervni_deo_ukupno) }}
				@endif
			</div>
		</div>
	</div>

	@if(RmaOptions::gnrl_options(3041))
 	<div class="row">
 		<div class="flat-box">
			<h3 class="title-med">Troškovi uslužnog servisa</h3>
			<div class="columns medium-9">
				<table>
					<tr>
						<th>Partner</th>
						<th>Opis</th>
						<th>Datum računa</th>
						<th>Iznos računa</th>
						<th>Dokumenti</th>
						<td></td>
					</tr>
					@foreach($radni_nalog_troskovi as $row)
					<tr>
						<td>{{ RMA::partner($row->partner_id)->naziv }}</td>
						<td>{{ $row->opis }}</td>
						<td>{{ $row->datum_racuna }}</td>
						<td>{{ $row->vrednost_racuna }}</td>
						<td><a class="edit icon" href="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokumenti/{{$radni_nalog->radni_nalog_id}}/{{$row->radni_nalog_trosak_id}}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Dokumenti</a></td>
						<td class="table-col-icons text-right">
							<a class="edit icon JSEditRNTrosak" data-radni_nalog_id="{{ $row->radni_nalog_id }}" data-radni_nalog_trosak_id="{{ $row->radni_nalog_trosak_id }}" href="#"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Izmeni</a>
							<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog-trosak/{{ $row->radni_nalog_trosak_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;Obriši</a>
						</td>
					</tr>
					@endforeach 
				</table> 
			</div>

			<div class="columns medium-3">
				<a class="btn btn-primary btn-small edit icon JSEditRNTrosak" data-radni_nalog_id="{{ $radni_nalog->radni_nalog_id }}" data-radni_nalog_trosak_id="0" href="#">Dodaj</a>
				<br>
				UKUPNO: {{ RMA::format_cene($radni_nalog_trosak_ukupno->osnovica) }} + {{ RMA::format_cene($radni_nalog_trosak_ukupno->pdv) }} (PDV 20%) = {{ RMA::format_cene($radni_nalog_trosak_ukupno->ukupno) }}
			</div>
		</div>
	</div>
	@endif

	<div class="row">
 		<div class="flat-box">
			<form method="POST" action="{{ RmaOptions::base_url() }}rma/radni-nalog-opis-rada-post">
				<input type="hidden" name="radni_nalog_id" value="{{ $radni_nalog->radni_nalog_id }}">
				
				<div class="columns medium-9">
					<h3 class="title-med">Uzrok kvara</h3>
					<textarea class="margin-bottom {{ $errors->first('uzrok_kvara') ? 'error' : '' }}" name="uzrok_kvara" >{{ Input::old('uzrok_kvara') ? Input::old('uzrok_kvara') : $radni_nalog->uzrok_kvara }}</textarea>
				</div>
				@if(RmaOptions::gnrl_options(3041))
				<div class="columns medium-3">
					UKUPNO: {{ RMA::format_cene($radni_nalog_ukupno) }}
				</div>
				@endif

				<div class="columns medium-9">
					<h3 class="title-med">Opis izvršenih radova</h3>
					<textarea class="margin-bottom {{ $errors->first('napomena_operacije') ? 'error' : '' }}" name="napomena_operacije" >{{ Input::old('napomena_operacije') ? Input::old('napomena_operacije') : $radni_nalog->napomena_operacije }}</textarea>
				</div>

				<div class="columns medium-4">
					<label class="inline-block" for="">Otpis</label>
					<input name="otpis" class="datum-val has-tooltip {{ $errors->first('otpis') ? 'error' : '' }}" type="checkbox" {{ (!is_null(Input::old('otpis')) && Input::old('otpis') == 'on') ? 'checked' : ($radni_nalog->otpis == 1 ? 'checked' : '') }} >
				</div>
				<div class="columns medium-4">
					<label class="inline-block" for="">Roba sa oštećenjem</label>
					<input name="ostecen" class="datum-val has-tooltip {{ $errors->first('ostecen') ? 'error' : '' }}" type="checkbox" {{ (!is_null(Input::old('ostecen')) && Input::old('ostecen') == 'on') ? 'checked' : ($radni_nalog->ostecen == 1 ? 'checked' : '') }} >
				</div>

				<div class="columns medium-9 large-9 text-center">
					<div class="btn-container"> 
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
				</div>				
			</form>
		</div>
	</div> 

	<div id="JSEditRNOperacijaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">Operacija radnog naloga</h4> 
		<div id="JSEditRNOperacijaModalContent" class="modal-content column medium-12"></div>
	</div>

	<div id="JSEditRNRezervniDeoModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">Utrošeni rezervni deo</h4> 
		<div id="JSEditRNRezervniDeoModalContent" class="modal-content column medium-12"></div>
	</div>

	<div id="JSEditRNTrosakModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">Troškovi uslužnog servisa</h4> 
		<div id="JSEditRNTrosakModalContent" class="modal-content column medium-12"></div>
	</div> 
</section>
@endsection