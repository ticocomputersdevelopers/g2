
<div>
	<input id="JSOperacijaCenaSata" type="hidden" value="{{ $radni_nalog_operacije->cena_sata }}">
	<div class="row">
		<div class="medium-6 columns">
			<label for="">Serviser</label>
			<select id="JSServiserId" >
				{{ RMA::serviseriSelect($radni_nalog->partner_id,$radni_nalog_operacije->serviser_id)}}
			</select>
		</div>
		<div class="medium-6 columns">
			<label for="">Grupa Operacije</label>
			<select id="JSGrupaOperacijeSelect" data-partner_id="{{ $radni_nalog->partner_id }}">
				{{ RMA::grupaOperacijeSelect($radni_nalog->partner_id,$radni_nalog_operacije->operacija_servis_id)}}
			</select>
		</div> 
	</div>

	<div class="row">
		<div class="medium-6 columns">
			<label for="">Operacija</label>
			<select id="JSOperacijeSelect" data-partner_id="{{ $radni_nalog->partner_id }}">
				{{ RMA::operacijeSelect($radni_nalog->partner_id,null,$radni_nalog_operacije->operacija_servis_id)->select_content }}
			</select>
		</div>
		<div class="medium-6 columns">
			<label for="">Opis operacija</label>
			<input type="text" id="JSOpisOperacije" value="{{ $radni_nalog_operacije->opis_operacije }}">
		</div>
	</div>

	<div class="row">
		<div class="medium-6 columns">
			<label for="">Kolicina</label>
			<input type="text" id="JSRNONormaSat" value="{{ $radni_nalog_operacije->norma_sat }}">
		</div>
		<div class="medium-6 columns">
			<label for="">Iznos</label>
			<input type="text" id="JSRNOIznos" value="{{ $radni_nalog_operacije->iznos }}">
		</div>
	</div>

	<div class="btn-container center"> <br>
		<button id="JSRNOperacijaSubmit" class="save-it-btn btn btn-primary" data-radni_nalog_operacije_id="{{$radni_nalog_operacije->radni_nalog_operacije_id}}" data-radni_nalog_id="{{$radni_nalog->radni_nalog_id}}" data-partner_id="{{ $radni_nalog->partner_id }}">Sačuvaj</button>
 	</div>
</div>