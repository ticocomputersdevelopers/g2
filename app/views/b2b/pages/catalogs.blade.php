@extends('b2b.templates.main')

@section('content')

<div class="row">
	<div class="col-xs-12">
	@foreach($catalogs as $row)  
		<div class="row"> 		
			<div class="col-md-8 col-sm-8 col-xs-12"> 		
				<h3><span>{{ $row->naziv }}</span></h3>		 
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 text-right"> 	
				<a href="{{ B2bOptions::base_url() }}b2b/katalog/{{ $row->katalog_id }}" target="_blank" class="button">{{ Language::trans('Preuzmi katalog') }}</a>
			</div> 
		</div>
	@endforeach
	</div>
</div>
@endsection