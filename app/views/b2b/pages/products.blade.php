@extends('b2b.templates.main')
@section('content')  

<!-- products.blade -->

<ul class="breadcrumb">
    @if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
        {{ B2bUrl::b2bBreadcrumbs()}}
    @elseif($strana == 'proizvodjac')
        <li><a href="{{ Options::base_url() }}b2b/brendovi">Brendovi</a></li>
        @if($grupa)
        <li><a href="{{ Options::base_url() }}b2b/proizvodjac/{{ B2bUrl::slugify($proizvodjac) }}">{{ B2bCommon::get_manofacture_name($proizvodjac_id) }}</a></li>
        <li>{{ !is_null($grupa_pr_id) ? B2bCommon::grupa_title($grupa_pr_id) : '' }}</li>
        @else
            <li>{{ B2bCommon::get_manofacture_name($proizvodjac_id) }} </li>
        @endif
    @else                   
        <li><a href="{{ B2bOptions::base_url() }}b2b">{{ All::get_title_page_start() }}</a></li>
        <li>{{$title}}</li>
    @endif
</ul> 
 

<div class="row"> 

    <div class="col-md-3 col-sm-3 col-xs-12 all-filters"> 
        @if(Route::getCurrentRoute()->getName()=='products_first' or  Route::getCurrentRoute()->getName()=='products_second' or Route::getCurrentRoute()->getName() == 'products_third' or Route::getCurrentRoute()->getName() == 'products_fourth')
            @include('b2b.partials.filters')
        @elseif($strana == 'proizvodjac')
            @include('b2b.partials.manufacturer_category')
        @endif
    </div>

    <div @if($strana != 'proizvodjac' and !isset($filters) and !isset($filtersItems) and !isset($proizvodjac_id))) class="col-md-12 col-sm-12 col-xs-12"  @else class="col-md-9 col-sm-9 col-xs-12" @endif >
  

        @if($strana != 'proizvodjac' && isset($sub_cats) and !empty($sub_cats) and $url)
        <div class="row sub-group">
             
            @foreach($sub_cats as $row)
            <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
                <a class="flex" href="{{ B2bOptions::base_url()}}b2b/artikli/{{ $url }}/{{ B2bUrl::slugify($row->grupa) }}">
                    @if(isset($row->putanja_slika))
                    
                        <img src="{{ B2bOptions::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class="img-responsive" /> 
                        
                    @endif
                   
                    <span>{{ $row->grupa }}</span>
                </a>
            </div>
            @endforeach
            
        </div>
        @endif
        <div class="row product-list-options">  <!-- ================= PRODUCT INFO OPTIONS ===================== -->
            <div class="col-md-7 col-sm-8 col-xs-12 sm-text-center"> 
                <span>Ukupno: {{ $count_products }} </span>

                @if(B2bOptions::product_number()==1)
                    <div class="dropdown">
                        <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown"> 
                            @if(Session::has('limit'))
                                {{Session::get('limit')}}
                            @else
                                20
                            @endif
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu currency-list">
                            <li><a href="{{ B2bOptions::base_url() }}b2b/limit/20">20</a></li>
                            <li><a href="{{ B2bOptions::base_url() }}b2b/limit/30">30</a></li>
                            <li><a href="{{ B2bOptions::base_url() }}b2b/limit/50">50</a></li>
                        </ul>
                    </div>
                @endif

                @if(B2bOptions::product_sort()==1)
                    <div class="dropdown"> 
                        <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
                            {{B2bCommon::get_sort()}} 
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu currency-list">
                            @if(B2bOptions::web_options(207) == 0)
                            <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                            <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                            @else
                            <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                            <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                            @endif
                            <li><a href="{{route('b2b.ordering',['news'])}}">Najnovije</a></li>
                            <li><a href="{{route('b2b.ordering',['name'])}}">Prema nazivu</a></li>
                        </ul>
                    </div>
                @endif 
               
                @if(B2bArticle::b2bproduct_view()==1)
                    <div class="view-buttons"> 
                        <a href="{{ B2bOptions::base_url() }}b2b/prikaz/table">
                            <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'table' OR !Session::has('b2b_prikaz')) ? 'active' : '' }}">
                                 <i class="fas fa-bars"></i> 
                            </span>
                        </a>
                        <a href="{{ B2bOptions::base_url() }}b2b/prikaz/list">
                            <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'list') ? 'active' : '' }}">
                                <i class="fas fa-list"></i>
                            </span>
                        </a>
                        <a href="{{ B2bOptions::base_url() }}b2b/prikaz/grid">
                            <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'grid') ? 'active' : '' }}">
                                <i class="fas fa-th"></i>
                            </span>
                        </a>       
                    </div>
                @endif  
            </div>
           
            
<!-- ========= PAGINATION ==============-->
            <div class="col-md-5 col-sm-4 col-xs-12 text-right sm-text-center"> 
                @if(Route::getCurrentRoute()->getName()=='b2b.search')
                    {{ $query_products->appends(['q'=>Input::get('q')])->links() }}
                @else
                    {{ $query_products->links() }}
                @endif
            </div>
        </div>  

        <div class="row">       <!-- ============ PRODUCT CONTENT ============== -->
            @if(Session::has('b2b_prikaz'))
                @if(Session::get('b2b_prikaz') == 'table')
                     @include('b2b.partials/product_on_table')      
                @elseif(Session::get('b2b_prikaz') == 'list')
                    @include('b2b.partials/product_on_list')
                @elseif(Session::get('b2b_prikaz') == 'grid')
                @foreach($query_products as $row)
                    @include('b2b.partials/product_on_grid')
                @endforeach 
                @endif   
            @else
                @include('b2b.partials/product_on_table') 
            @endif

            @if($count_products == 0)
                <p class="empty-page"> Trenutno nema artikla za date kategorije</p> 
            @endif 
        </div>
<!--========== PAGINATION DOWN ========= -->
        <div class="row text-right"> 
            @if(Route::getCurrentRoute()->getName()=='b2b.search')
                {{ $query_products->appends(['q'=>Input::get('q')])->links() }}
            @else
                {{ $query_products->links() }}   
            @endif
        </div>
    </div>
</div>  

<!-- products.blade end -->
@endsection
 