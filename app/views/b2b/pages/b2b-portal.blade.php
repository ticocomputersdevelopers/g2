@extends('b2b.templates.login')
@section('content')
<div class="wrapper">

    <div class="wrapper-inner">
        <div class="row flex">
            <div class="col-md-5 col-sm-5 col-xs-12 text-center"> 
                <a href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                    <img class="logo" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="{{ B2bOptions::company_name()}}">
                </a>
            </div> 

            <div class="col-md-7 col-sm-7 col-xs-12 left-border">  
                <form action="{{route('b2b.login.store')}}" method="post" class="bg-fff">
                    <div class="row text-center">
                        <div class="col-xs-4 no-padd"> 
                            <p class="title">PRIJAVA</p>
                        </div>
                        <div class="col-xs-8 no-padd"> 
                            @if(!B2bOptions::info_sys('calculus'))
                            <a class="registration-btn" href="{{route('b2b.registration')}}">REGISTRACIJA</a>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="box-gap">
                        <label class="label" for="username">Korisničko ime</label>
                        <input id="username" name="username" type="text" class="form-control"> 
                        @if($errors->first('username'))
                        <span class="error">{{ $errors->first('username') }}</span>
                        @endif
                    </div>

                    <div class="box-gap">
                        <label class="label" for="password">Lozinka</label>
                        <input id="password" name="password" type="password" class="form-control"> 
                        @if($errors->first('password'))
                        <span class="error">{{ $errors->first('password') }}</span>
                        @endif
                    </div> 

                    <div class="box-gap">
                        <button type="submit" class="sign-in-btn" onclick="login()">PRIJAVI SE</button>&nbsp;
                        @if(!B2bOptions::info_sys('wings') AND !B2bOptions::info_sys('calculus'))
                        <a class="new-psw-btn" href="{{ route('b2b.forgot_password') }}">Zaboravljena lozinka</a>
                        @endif 
                    </div> 
                </form> 
            </div> 
        </div>
    
        <div class="text-right"> 
            <a class="logo-selltico" href="https://www.selltico.com/" target="_blank"> 
                B2B ENGINE BY: 
                <img src="{{ Options::base_url()}}images/logo-selltico.png">    
            </a>
       </div>  
   </div>

</div>
@endsection