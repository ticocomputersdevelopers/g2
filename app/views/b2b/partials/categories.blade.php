<div class="category-nav relative JScategories">

	<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
  
    <h3 class="text-center">Kategorije <i class="fas fa-bars"></i>
    	<span class="JSclose-nav hidden-md hidden-lg">&times;</span>
	</h3> 
	 
	<!-- CATEGORIES LEVEL 1 -->

    <ul class="JSlevel-1"> 
    @if(B2bOptions::category_type()==1) 
		@foreach ($query_category_first as $row1)
			@if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)

			<li>
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}">
					@if(B2bCommon::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block hidden-sm hidden-xs"> 
						<img src="{{ B2bOptions::base_url() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
				 	</span>
					@endif

					{{$row1->grupa}}
				</a>
				 
				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="JSlevel-2 row">
			 		@foreach ($query_category_second->get() as $row2)
					<li class="col-md-6 col-sm-12 col-xs-12">  
					 
					 	<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}">

							@if(B2bCommon::check_image_grupa($row2->putanja_slika))
								<span class="lvl-2-cont inline-block hidden-sm hidden-xs">
									<img src="{{ B2bOptions::base_url() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
								</span>
							@endif 
						  
							{{$row2->grupa}}
						</a> 
							   
						@if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
						<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
						
						<ul class="JSlevel-3">
							@foreach($query_category_third as $row3)
							
							<li>						 
								<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}/{{ B2bUrl::slugify($row3->grupa) }}">
									{{$row3->grupa}}
								</a>

<!-- 								@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend">
									<i class="fa fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif   -->
 
								@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
											
								<ul class="JSlevel-4">
									@foreach($query_category_forth as $row4)
									<li>
										<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}/{{ B2bUrl::slugify($row3->grupa) }}/{{ B2bUrl::slugify($row4->grupa) }}">{{$row4->grupa}}</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
				</ul>
			</li>

			@else

			<li>		 
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}">		 
					@if(B2bCommon::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont hidden-sm hidden-xs"> 
						<img src="{{ B2bOptions::base_url() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{$row1->grupa}}  				
				</a>			 
			</li>
			@endif
		@endforeach
	@else
		@foreach ($query_category_first as $row1)
			 
		@if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
		  	<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}">
				{{$row1->grupa}}
			</a>
		<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc') ?>
 
		@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}">
					{{$row2->grupa}}
				</a>
			@if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
			<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul>
				@foreach($query_category_third as $row3)
				<li> 
					<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}/{{ B2bUrl::slugify($row3->grupa) }}">{{$row3->grupa}}
					</a>
				</li>
				@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
				<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

				<ul>
					<li> 
						@foreach($query_category_forth as $row4)
						<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}/{{ B2bUrl::slugify($row2->grupa) }}/{{ B2bUrl::slugify($row3->grupa) }}/{{ B2bUrl::slugify($row4->grupa) }}">{{$row4->grupa}}</a>
						@endforeach
					</li>
				</ul>
				@endif	
				@endforeach
				</ul>
			@endif
			</li>
		@endforeach

		@else
			<li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::slugify($row1->grupa) }}">{{$row1->grupa}}</a></li>
		@endif
  
		@endforeach				
	@endif
 
</ul>
 
</div>
