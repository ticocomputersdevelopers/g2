@extends('b2b.layouts.b2bpage')
@section('content')
<section class="news-section">
	<article class="single-news">
		<div class="row">
			<div class="columns medium-12 large-12">
				<img class="vest-slika" src="{{ B2bOptions::base_url() . $vest->slika }}" alt="">
			</div>
			<div class="columns medium-12 large-12">
				<h2 class="news-title">{{ $vest->naslov }}</h2>
				<p>{{ $vest->sadrzaj }}</p>

			</div>
		</div> <!--  end of .row -->
	</article>
</section>
@endsection