<!-- MENU_TOP.blade -->
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal" rel="nofollow"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave" rel="nofollow"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin" rel="nofollow"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div class="preheader sm-text-center">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container"> 
        <div class="row top-menu flex">

            <div class="col-md-7 col-sm-7 col-xs-12 border-bottom-sm">     
                
                <!-- @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block text-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif  -->

                <ul class="hidden-small JStoggle-content hidden">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a class="center-block" href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

                <!-- Contact Data -->
                <div class="preheader-contact">
                    <a href="tel: +381 11 3077 525">
                        <i class="fas fa-phone"></i>
                        +381 11 3077 525
                    </a>
                    <a href="tel: +381 60 3100 216">
                        <i class="fas fa-mobile-alt"></i>
                        +381 11 3077 525
                    </a>
                    <a href="mailto: g2labor@gmail.com">
                       <i class="fas fa-envelope"></i>
                       g2labor@gmail.com
                    </a>
                </div>

            </div>

            <div class="col-md-5 col-sm-5 col-xs-12 text-center" id="preheader-icons"> 
                <!-- @if(Options::checkB2B())
                <a href="{{Options::domain()}}b2b/login" class="center-block" rel="nofollow">B2B</a> 
                @endif  -->
                <div class="preheader-icons text-right sm-text-center">  
                    <a href="https://facebook.com/g2"><i class="fab fa-facebook-f"></i> <span class="tooltip"><i class="fas fa-caret-down"></i> Facebook</span> </a>
                    <a href="https://instagram.com/g2"><i class="fab fa-instagram"></i> <span class="tooltip"><i class="fas fa-caret-down"></i> Instagram</span></a>
                </div>
            </div>   
 
        </div> 
    </div>
</div>
<!-- MENU_TOP.blade END -->



