<div class="col-md-2 col-sm-2 col-xs-3 padding-x-35 switch-toggle">
	
	<div class="header-cart-container relative">  
		
		<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
			
			<i class="fas fa-cart-plus"></i>		
			
			<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		
			
			<!-- <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	 -->

			<span>
				@if(Options::checkB2B())
					<a href="{{Options::domain()}}b2b/login" class="center-block" rel="nofollow">B2B Portal</a> 
				@endif 
			</span>
			
		</a>

		<div class="JSheader-cart-content hidden-sm hidden-xs">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
</div> 