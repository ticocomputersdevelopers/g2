<?php // echo $kara; ?>
<div class="filters row"> 
<!-- CHECKED FILTERS -->
   		<div class="col-xs-12">
   			<br>

		    <div class="clearfix JShidden-if-no-filters text-center">
		    	<a class="JSreset-filters-button" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="far fa-trash-alt"></span></a> <br>
		    	<span>{{ Language::trans('Izabrani filteri') }}:</span>
		    </div> 
			
			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
					    {{All::get_manofacture_name($row)}}     
					    <a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
				<?php }}

				$br=0;
				foreach(All::getGroupedCharac($niz_karakteristike) as $key => $val){
					$br+=1;
					if($val != 0){
				?>
					<li>
						{{Language::trans($key)}}
						<a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$val}}"></span>
					    </a>
					</li>
					<?php }}?>
			</ul>
			<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>
		</div>	

	  
	<!-- END CHECKED -->
	  		<ul class="col-md-12 col-sm-12 col-xs-12"> 

			    <!-- FILTER ITEM -->
			    @if(count($manufacturers)>0)
				<li class="filter-box">
                    <a href="javascript:void(0)" class="filter-links flex JSfilters-slide-toggle" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
                    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
                   <i class="fas fa-angle-down"></i></a>
				 	 	<div class="JSfilters-slide-toggle-content">
							@foreach($manufacturers as $key => $value)
							@if(in_array($key,$niz_proiz))
								<label class="flex">
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@else
								<label class="flex">
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@endif
							@endforeach
						</div>				 				 
					</li>
				@endif

				@foreach($characteristics as $keys => $values)
				<li class="filter-box">
	 				<a  href="javascript:void(0)" class="filter-links flex JSfilters-slide-toggle" rel=”nofollow”>
						{{Language::trans($keys)}} 
						<!-- <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
						<i class="fas fa-angle-down"></i>  							
					</a>
					<div class="JSfilters-slide-toggle-content">
						@foreach($values as $key => $value)
						<?php if(!array_diff(explode('-',$value['ids']),$niz_karakteristike)){ ?>
						<label class="flex">
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}" checked>
							<span class="filter-text">
								{{ Language::trans($key) }}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value['count'] }} @endif
							</span>
						</label>
						<?php }else {?>
						<label class="flex">
							<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}"> 
							<span class="filter-text">
								{{ Language::trans($key) }}
							</span>
							<span class="filter-count">
								@if(Options::filters_type()) {{ $value['count'] }} @endif
							</span>
						</label>
						<?php } ?>
						@endforeach
					</div>
				</li>
				@endforeach			
            </ul>	 
		<input type="hidden" id="JSfilters-url" value="{{$url}}" />
 
<!-- SLAJDER ZA CENU -->
	<div class="col-xs-12 text-center"> 
		<br>	
		<span id="JSamount" class="filter-price"></span>
		<div id="JSslider-range"></div><br>
	</div>
	<script>
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>
</div>

 
 