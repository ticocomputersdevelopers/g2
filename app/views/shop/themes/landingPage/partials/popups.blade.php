
<div class="popup info-confirm-popup JSinfo-popup">
    <div class="popup-wrapper">	
	    <div class="JSpopup-inner">
		@if($firstSlider = Slider::getFirstSlider('Popup Baner') AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
			<div class="JSfirst-popup first-popup">
				<div class="first-popup-inner relative">  
					 <a href="{{ $slajderStavke[0]->link }}" class="relative inline-block" target="_blank">  
					 	<img class="popup-img" src="{{AdminOptions::base_url()}}{{$slajderStavke[0]->image_path}}" alt="{{AdminOptions::base_url()}}{{$slajderStavke[0]->image_path}}">	
					 </a> 
					 <span class='JSclose-me-please'>&times;</span>
			 	</div>
			</div>
		@endif
	    </div>	
	</div>
</div>
