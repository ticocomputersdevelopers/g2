
@if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1)
<!-- Drip -->
<script>
  var _dcq = _dcq || [];
  var _dcs = _dcs || {};
  _dcs.account = '3226034';

  (function() {
    var dc = document.createElement('script');
    dc.type = 'text/javascript'; dc.async = true;
    dc.src = '//tag.getdrip.com/3226034.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(dc, s);
  })();
</script>
<!-- end Drip -->
@endif