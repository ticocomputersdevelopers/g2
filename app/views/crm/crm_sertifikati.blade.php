@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section id="main-content">
	@include('crm/partials/crm_tabs')
	<div class='row'>
				<div class="columns medium-3"> 
					<!-- SEARCH -->
					<div class="m-input-and-button">
						<input type="text" name="search" value="{{ urldecode($search) }}" placeholder={{ AdminLanguage::transAdmin('Pretraga...') }} >

						<button id='JSCRMfakture' class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Pretraga') }}</button>

						<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}crm/crm_fakture">{{ AdminLanguage::transAdmin('Poništi') }}</a>
					</div>  
				</div> 
				<!-- <div class="columns medium-1">
					<input name="datum_pocetka" type="text" value="{{ $datum_pocetka }}" autocomplete="on" placeholder="Datum od">
				</div>

				<div class="columns medium-1">
					<input name="datum_kraja" type="text" value="{{ $datum_kraja }}" autocomplete="on" placeholder="Datum do">
				</div>
				<button aria-label="Poništi" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_fakture">
								<i class="fa fa-times"  aria-hidden="true"></i>
							</a>
						</button>

 -->
					 <div class="columns medium-2">
						<select name="crm_tip_id">
							<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi vrstu ssla') }}</option>
							<option>{{ AdminCrm::tipoviSelect($tip_ids) }}</option>
						</select>
						<button id="JSTipoviSearch" class="btn btn-primary btn-abs-right">
							<i class="fa fa-check" area-hidden="true"></i>
						</button>
					</div>
					<button aria-label={{ AdminLanguage::transAdmin('Poništi') }} class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_sertifikati"><i class="fa fa-times" aria-hidden="true"></i>
							</a>
					</button>
		</div>
			

				<div class="">
					<label>{{ AdminLanguage::transAdmin('Ukupno') }}: <b>{{count($sertifikati)}}</b> </label> 
					<table class="fixed-table-header" id="JSmyTable">
						<thead class="table-head">
							<tr>
								
								<th>{{ AdminLanguage::transAdmin('Partner') }}</th>
								<th>{{ AdminLanguage::transAdmin('Vrsta') }} {{ AdminLanguage::transAdmin('SSL-a') }}</th>  	
								<th>{{ AdminLanguage::transAdmin('Datum')}}  {{'isteka'}}</th>
								<th></th>
								<th></th>
							
							</tr>
						</thead>

						<tbody> 

							@foreach($sertifikati as $row)

							<tr>
								<form action="{{AdminOptions::base_url()}}crm/crm_sertifikati_save/{{$row->crm_sertifikati_id}}"  method="POST" autocomplete="false">
									<input type='hidden' value="{{$row->crm_sertifikati_id}}" name='crm_sertifikati_id'>
									<td><input type='hidden' value="{{$row->partner_id}}" name='partner_id'>{{AdminB2BProizvodjac::getPartnerNaziv($row->partner_id)}} </td>
									<td><input type='text' value="{{$row->opis}}" name='opis'> </td>
									<td class="datepicker-col__box"><input type='text' class="JSdatepickerAction" value="{{ date('d-m-Y',strtotime($row->datum_sertifikata)) }}" name='datum_sertifikata'>
									</td>
									<td><button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i>
									</button></td>
									<td> 
										<a href="{{ AdminOptions::base_url() }}crm/crm_sertifikati_delete/{{ $row->crm_sertifikati_id }}" onclick="return confirm('Jeste li sigurni da želite da obrišete podatke')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}"><i class="fa fa-times red"></i></a>
									</td>
								
								</form>
							</tr>
							@endforeach

						</tbody>
					</table>
					
						  
				</div>
	
</section>
<script type="text/javascript">
         document.addEventListener("DOMContentLoaded", function(event) { 
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) window.scrollTo(0, scrollpos);
        });

        window.onbeforeunload = function(e) {
            localStorage.setItem('scrollpos', window.scrollY);
        };
  </script>		