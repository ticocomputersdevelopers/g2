<br>
@if(Session::has('message'))
<script>
	alertify.success('{{ Session::get('message') }}');
</script>
@endif

<section id="main-content">
@include('crm/partials/crm_tabs')
	<div class="row">
		<div class="columns medium-3"> 
			<!-- SEARCH -->
				<div class="m-input-and-button">
					<input type="text" name="search" value="{{ urldecode($search) }}" placeholder={{ AdminLanguage::transAdmin('Pretraga...') }} >
						<button id="JSCRMSearch" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Pretraga') }}</button>
						<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}crm/crm_logovi">{{ AdminLanguage::transAdmin('Poništi') }}</a>
				</div>  
		</div> 

		<!-- filteri --> 
		<div class="columns medium-2">				
			<select name="crm_logovi_akcija">
				<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi akciju') }}</option>
				<option value="Izmena LEAD">{{ AdminLanguage::transAdmin('Izmena LEAD') }}</option>
				<option value="Izmena AKCIJE">{{ AdminLanguage::transAdmin('Izmena AKCIJE') }}</option>
				<option value="Kreiran lead">{{ AdminLanguage::transAdmin('Kreiran lead') }}</option>
				<option value="Brisanje">{{ AdminLanguage::transAdmin('Brisanje') }}</option>
				<option value="Zavrsena akcija">{{ AdminLanguage::transAdmin('Zavrsena akcija') }}</option>
				<option value="Dodavanje">{{ AdminLanguage::transAdmin('Dodavanje') }}</option>
				<option value="Poslata faktura">{{ AdminLanguage::transAdmin('Poslata faktura') }}</option>
			</select>
			<button id="JSLogoviSearch" class="btn btn-primary btn-abs-right">
				<i class="fa fa-check" area-hidden="true"></i>
			</button>
		</div>
		<div class="columns medium-2">				
			<select name="imenik_id">
				<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi korisnika') }}</option>
				@foreach($korisnici as $log)
				<option value="{{$log->imenik_id}}">{{$log->ime}}</option>
				@endforeach
			</select>
			<button id="JSKorisnikSearch" class="btn btn-primary btn-abs-right">
				<i class="fa fa-check" area-hidden="true"></i>
			</button>
		</div>


		
			<div class="columns medium-1">
				<input name="datum_pocetka" type="text" value="{{ $datum_pocetka }}" autocomplete="on" placeholder="Datum od">
			</div>

				<div class="columns medium-1">
					<input name="datum_kraja" type="text" value="{{ $datum_kraja }}" autocomplete="on" placeholder="Datum do">
				</div>
				<button aria-label="Poništi" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
					<a href="{{AdminOptions::base_url()}}crm/crm_logovi">
						<i class="fa fa-times"  aria-hidden="true"></i>
					</a>
				</button>
	</div>

					<h3>{{ AdminLanguage::transAdmin('Svi logovi') }}</h3>
					<h5>{{ AdminLanguage::transAdmin('Ukupno:') }} {{count($logovi)}}</h5>
					<table class="table-scroll tap-to-close">	
						<thead>
							<tr>
								<th>{{ AdminLanguage::transAdmin('') }}</th>
								<th>{{ AdminLanguage::transAdmin('Ime') }}</th>
								<th>{{ AdminLanguage::transAdmin('Email') }}</th>
								<th>{{ AdminLanguage::transAdmin('Datum izmene') }}</th>
								<th>{{ AdminLanguage::transAdmin('Akcija') }}</th>
								<th>{{ AdminLanguage::transAdmin('Stara vrednost') }}</th>
								<th>{{ AdminLanguage::transAdmin('Nova vrednost ') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($logovi as $kom)
							<tr>
								<td>{{$nmb++}}</td>	
								<td>{{$kom->ime}}</td>
								<td>{{$kom->prezime}}</td>
								<td>{{$kom->datum_izmene}}</td>
								<td>{{$kom->akcija}}</td>
							
								<td>{{($kom->stara_vrednost)}}</td>
								<td>{{$kom->nova_vrednost}}</td>								
							</tr>
							@endforeach
						</tbody>
					</table>
</section>