<div id="main-content">

	@include('admin/partials/product-tabs')

	<div class="row">
		<div class="column medium-6 medium-centered">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/seo_edit">
				<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id ? $jezik_id : 0 }}">

				<div class="flat-box">

					@if(count($jezici) > 1)
						@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) 
						<div class="languages">
							<ul>	
								@foreach($jezici as $jezik)
								<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/product_seo/{{ $roba_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
							</ul>
						</div> 
						@endif
					@endif    

					<div class="row">
						<div class="columns"> 

							<h3 class="center seo-info tooltipz margin-h-10" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>

							<label>{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}</label>
							<div class="field-group">
								<input type="text" class="{{ $errors->first('seo_title') ? 'error' : '' }}" name="seo_title" value="{{ Input::old('seo_title') ? Input::old('seo_title') : $seo_title }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>	

							<label>{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }} </label>
							<div class="field-group">
								<textarea name="description" class="{{ $errors->first('description') ? 'error' : '' }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('description') ? Input::old('description') : $description }}</textarea>
							</div> 

							<label>{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
							<div class="field-group">
								<textarea name="keywords" class="{{ $errors->first('keywords') ? 'error' : '' }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}</textarea>
							</div>	 

							<label>{{ AdminLanguage::transAdmin('Tagovi (Default-ni jezik)') }}</label>
							<div class="field-group">
								<textarea name="tags" class="{{ $errors->first('tags') ? 'error' : '' }}" {{($jezik_id != 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('tags') ? Input::old('tags') : $tags }}</textarea> 
							</div>	      
						</div>
					</div>
			 
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<div class="btn-container center">
						<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
					@endif
				</div>
			</form>		
		</div>
	</div>
</div> 