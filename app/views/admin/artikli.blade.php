<section class="articles-page" id="main-content">
	
    <div class="row collapse">
		<div class="medium-2 columns JScat_toggle" hidden="hidden">
			
			@include('admin.partials.kat')
		
		</div>
		<!-- DO NOT REMOVE CLASSES -->
   		<div class="medium-12 columns JStoggle_product_list">

			@include('admin/product_list')

		</div>
	</div> 

<!-- ARTIKLI.BLADE -->
	@include('admin.partials.custom_menu')
</section> 