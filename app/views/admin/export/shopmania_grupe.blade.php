<html>
	<head>
		<title>Shopmania grupe</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <style>
        	body {
        		margin-top: 20px;
        	}
        	.warn {
        		color: red !important;
        		-moz-color: red !important;
        		background: #aaa;
        	}
        	.error {
        		color: red;
        	}
        	.btn-custom {
        		cursor: pointer;
        		width: 100px;
        		padding: 5px;
        		text-align: center;
        		background-color: #0000ffb3;
        		border-radius: 5px;
        	}
        	.btn-custom:hover {
        		background-color: #0000ff80;
        	}
        	#JSObrisi {
        		background-color: #f009;
        	}
        	#JSObrisi:hover {
        		background-color: #ff0000e6;
        	}
        	.padd {
        		padding-top: 100px;
        	}
        </style>
	</head>
	<body>
		<a href="{{ AdminOptions::base_url()}}export/shopmania/{{ $kind }}/{{$key}}" class="btn btn-primary">Nazad</a>
		<form method="POST" action="{{AdminOptions::base_url()}}export/shopmania-grupe-povezi/{{ $key }}" id="JSPovezivanjeGrupa">
			<input type="hidden" name="base_url" value="{{ AdminOptions::base_url() }}">
			<input type="hidden" name="kind" value="{{ $kind }}">
			<input type="hidden" name="key" value="{{ $key }}">
			<div class="row">
				<div class="columns large-12">
				<h3>Povezivanje grupa</h3>
					<div class="columns large-6">
						Shopmania grupe				
						<select name="shopmania_grupa_id" id="shopmania_grupa_id" class="m-input-and-button select2shopmania">						
							{{ AdminSupport::selectGroupsShopmania(Input::old('shopmania_grupa_id')!=null?Input::old('shopmania_grupa_id'):null) }}
						</select>
						<div class="error" id="JSObrisiError">{{ $errors->first('shopmania_grupa_id') ? $errors->first('shopmania_grupa_id') : '' }}</div>
					</div>
					<div class="columns large-6">
						Grupe u prodavnici				
						<select name="grupa_pr_id" class="m-input-and-button select2grupa">						
							{{ AdminSupport::selectGroupsSm($shopmania_grupe,Input::old('grupa_pr_id')!=null?Input::old('grupa_pr_id'):null) }}
						</select>
						<div class="error">{{ $errors->first('grupa_pr_id') ? $errors->first('grupa_pr_id') : '' }}</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="columns large-12">
					<div class="columns large-6">
						<div class="btn-custom" id="JSObrisi">Razveži</div>
					</div>
					<div class="columns large-6">
						<div class="btn-custom" id="JSPovezi">Poveži</div>
					</div>
				</div>
		</form>
			</div>
			<div class="row padd">
				<form method="POST" action="{{AdminOptions::base_url()}}export/shopmania-grupa-file/{{ $key }}" enctype="multipart/form-data" id="JSUcitavanjeFajla">
					<input type="hidden" name="kind" value="{{ $kind }}">
					<input type="hidden" name="key" value="{{ $key }}">	
					<div class="columns large-12">
						<h3>Učitavanje fajla za shopmania grupe (ekstenzija: '.ods', delimetar: '>')</h3>
						<div class="columns large-5">
							<input type="file" name="imp_file" id="imp_file" value="{{ Input::old('imp_file')!=null?Input::old('imp_file'):null }}">
							@if(Session::has('message'))
								<div>{{ Session::get('message') }}</div>
							@endif
						</div>
					</div>
				</form>
			</div>
			<div class="row padd">
				<form method="POST" action="{{AdminOptions::base_url()}}export/shopmania-grupa-dodaj/{{ $key }}" id="JSDodavanjeGrupe">
					<input type="hidden" name="kind" value="{{ $kind }}">
					<input type="hidden" name="key" value="{{ $key }}">	
					<div class="columns large-12">
						<h3>Kreiranje nove grupe za shopmaniju</h3>
						<div class="columns large-5">
							<input type="text" name="naziv" value="{{ Input::old('naziv')!=null?Input::old('naziv'):null }}">
							<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div>
						</div>
						<div class="columns large-5">			
							<select name="parent_id" class="m-input-and-button">						
								{{ AdminSupport::selectParentGroupsShopmania(Input::old('parent_id')!=null?Input::old('parent_id'):null) }}
							</select>
							<div class="error">{{ $errors->first('parent_id') ? $errors->first('parent_id') : '' }}</div>
						</div>
						<div class="columns large-2">
							<div class="btn-custom" id="JSDodajGrupu">Dodaj</div>
						</div>
					</div>
				</form>
			</div>


		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
		<script type="text/javascript">

			$(document).ready(function () {

				$(".select2shopmania").select2({
					placeholder: 'Izaberite grupu'
				});

				$(".select2grupa").select2({
					placeholder: 'Izaberite grupu'
				});

			});
		</script>

	</body>
</html>