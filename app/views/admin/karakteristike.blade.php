<div id="main-content" class="">
@include('admin/partials/product-tabs')
	@include('admin/partials/karak-tabs')
	
	<div class="row">
		<div class="columns medium-8 medium-centered">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/karakteristike_edit">
				<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
				  <div class="flat-box"> 
				     <div class="row">
						<div class="column medium-12"> 
							<br>
					     	<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif name="web_karakteristike" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $web_karakteristike }}</textarea>
					     	@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
								<div class="btn-container center">
									<a class="setting-button btn btn-primary" href="#" id="JSsablon">{{ AdminLanguage::transAdmin('Unesi šablon') }}</a>
									<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
								</div>
							@endif
						</div>
					</div>
				</div> <!-- end of .row -->
			</form>
		</div> <!-- end of .coluimn -->
	 </div> <!-- end of .row -->
 </div>