
@foreach($files as $file)
<div class="column large-2 medium-3 small-12 no-padd">
	<div class="padding-h-8 padding-v-8 margin-bottom col-slika col-document">
		<?php $ext=pathinfo($file, PATHINFO_EXTENSION); ?>
		@if($ext=='pdf')
				<div class="pointer text-center margin-h-10"> <i class="JSSelectFile fa fa-file-pdf-o" data-id="{{ $file }}"> </i></div>
		@elseif($ext=='xls' OR $ext=='xlsx')
				<div class="pointer text-center margin-h-10"> <i class="JSSelectFile fa fa-file-excel-o" data-id="{{ $file }}"> </i></div>
		@elseif($ext=='txt')
				<div class="pointer text-center margin-h-10"> <i class="JSSelectFile fa fa-file-text-o" data-id="{{ $file }}"> </i></div>
		@elseif($ext=='doc')
				<div class="pointer text-center margin-h-10"> <i class="JSSelectFile fa fa-file-word-o" data-id="{{ $file }}"> </i></div>
		@endif
		<span class="img-desc" title="{{ $file }}">{{ $file }}</span>
		<a class="btn btn-danger JSFileDelete" data-id="{{ $file }}"> {{ AdminLanguage::transAdmin('Obriši') }} </a>
	</div>
</div>
@endforeach
