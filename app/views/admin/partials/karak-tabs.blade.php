
	<div class="m-tabs clearfix">
		@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
		<div class="m-tabs__tab{{ $strana=='karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_karakteristike/{{ $roba_id }}">{{ AdminLanguage::transAdmin('HTML KARAKTERISTIKE') }} </a></div>
		@endif
		@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
		<div class="m-tabs__tab{{ $strana=='generisane' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_generisane/{{ $roba_id }}">{{ AdminLanguage::transAdmin('GENERISANE KARAKTERISTIKE') }}</a></div>
		@endif
		@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
		<div class="m-tabs__tab{{ $strana=='dobavljac_karakteristike' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/dobavljac_karakteristike/{{ $roba_id }}">{{ AdminLanguage::transAdmin('OD DOBAVLJAČA') }}</a></div>
		@endif
	</div>