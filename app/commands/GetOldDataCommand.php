<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class GetOldDataCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'get:old:data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		Config::set('database.connections.old_db', array(
			'driver'   => 'pgsql',
			'host'     => '78.46.228.58',
			'database' => 'fluo',
			'username' => 'postgres',
			'password' => 'Bc2Bfk7s',
			'charset'  => 'utf-8',
			'prefix'   => '',
			'schema'   => 'public',
			'port'     => '5432',
	    ));
	    $this->oldDB = DB::connection('old_db');
	}

	public static function convert($string){
		$string = pg_escape_string($string);
		// $string = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		return $string;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}

	public static function fileExtension($image_path){
		$extension = 'jpg';
    	$slash_parts = explode('/',$image_path);
    	$old_name = $slash_parts[count($slash_parts)-1];
    	$old_name_arr = explode('.',$old_name);
    	$extension = $old_name_arr[count($old_name_arr)-1];
    	return $extension;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		// roba
		$articles = $this->oldDB->select("SELECT * FROM roba where roba_id > -1");
		
		$article_rows="";
		foreach ($articles as $article) {
			$article_rows .= "('".self::convert($article->web_opis)."','".self::convert($article->web_karakteristike)."','".strval($article->sifra_d)."'),";
		}

		$table_temp = "(VALUES ".substr($article_rows,0,-1).") roba_temp(web_opis,web_karakteristike,id_is)";

		DB::statement("UPDATE roba r SET web_opis=roba_temp.web_opis, web_karakteristike=roba_temp.web_karakteristike FROM ".$table_temp." WHERE r.id_is = roba_temp.id_is");

	    $getMappedArticles = self::getMappedArticles();

	    //slike
		$slike = $this->oldDB->select("SELECT akcija, putanja, (SELECT sifra_d FROM roba r WHERE r.roba_id = ws.roba_id) AS sifra_d FROM web_slika ws WHERE (SELECT sifra_d FROM roba r WHERE r.roba_id = ws.roba_id) is not null");		
        foreach($slike as $slika){
            try {
            	if(isset($getMappedArticles[$slika->sifra_d]) && $getMappedArticles[$slika->sifra_d] != ''){
	            	$roba_id = $getMappedArticles[$slika->sifra_d];
	                $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
	                $ext = self::fileExtension($slika->putanja);
	                $name = $slika_id.'.'.$ext;
	                $destination = 'images/products/big/'.$name;
	                $url = 'http://fluoelektro.com/'.$slika->putanja;
	                $akcija = $slika->akcija;
	        	
	                $content = @file_get_contents($url);
	                file_put_contents($destination,$content);

	                $insert_data = array(
	                    'web_slika_id'=>intval($slika_id),
	                    'roba_id'=> $roba_id,
	                    'akcija'=>$akcija, 
	                    'flag_prikazi'=>1,
	                    'putanja'=>'images/products/big/'.$name
	                    );

	                DB::table('web_slika')->insert($insert_data);
	            }
            }
            catch (Exception $e) {
            }
        }


		//characteristics
		$characteristics = $this->oldDB->select("SELECT * FROM grupa_pr_naziv");
		if($characteristics){
			foreach($characteristics as $characteristic){
				$grupa_pr_naziv_id = DB::select("SELECT MAX(grupa_pr_naziv_id) + 1 AS next_id FROM grupa_pr_naziv")[0]->next_id;
				if(is_null($grupa_pr_naziv_id)){
					$grupa_pr_naziv_id = 1;
				}
				if(isset($this->oldDB->select("SELECT grupa FROM grupa_pr WHERE grupa_pr_id = ".$characteristic->grupa_pr_id."")[0])){
					$grupa_naziv = $this->oldDB->select("SELECT grupa FROM grupa_pr WHERE grupa_pr_id = ".$characteristic->grupa_pr_id."")[0]->grupa;
					$grupa_pr = DB::select("SELECT grupa_pr_id FROM grupa_pr WHERE grupa = '".self::convert($grupa_naziv)."'");

					if(isset($grupa_pr[0])){
						DB::statement("INSERT INTO grupa_pr_naziv (grupa_pr_naziv_id,grupa_pr_id,active,css,naziv,rbr) VALUES (".$grupa_pr_naziv_id.",".$grupa_pr[0]->grupa_pr_id.",".$characteristic->active.",'".$characteristic->css."','".self::convert($characteristic->naziv)."',".$characteristic->rbr.")");

						$characteristic_values = $this->oldDB->select("SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_naziv_id = ".$characteristic->grupa_pr_naziv_id."");
						if($characteristic_values){
							foreach($characteristic_values as $characteristic_value){
								DB::statement("INSERT INTO grupa_pr_vrednost (grupa_pr_naziv_id,active,css,naziv,rbr,boja) VALUES (".$grupa_pr_naziv_id.",".$characteristic_value->active.",'".$characteristic_value->css."','".self::convert($characteristic_value->naziv)."',".(!is_null($characteristic_value->rbr) ? $characteristic_value->rbr : '0').",'".$characteristic_value->boja."')");
							}
						}
					}
				}
			}
			if(count($characteristics) > 0){
				DB::statement("SELECT setval('grupa_pr_naziv_grupa_pr_naziv_id_seq', (SELECT MAX(grupa_pr_naziv_id)+1 FROM grupa_pr_naziv), FALSE)");
				DB::statement("SELECT setval('grupa_pr_vrednost_grupa_pr_vrednost_id_seq', (SELECT MAX(grupa_pr_vrednost_id)+1 FROM grupa_pr_vrednost), FALSE)");
			}
		}


		$article_characteristics = $this->oldDB->select("SELECT * FROM web_roba_karakteristike");
		if($article_characteristics){
			foreach($article_characteristics as $article_characteristic){
				if(isset($getMappedArticles[$article_characteristic->roba_id])){
					$group_characteristic_values = $this->oldDB->select("SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id = ".$article_characteristic->grupa_pr_vrednost_id."");
					if(isset($group_characteristic_values[0])){
						$characteristic_value = $group_characteristic_values[0]->naziv;
						$grupa_pr_vrednost = DB::select("SELECT * FROM grupa_pr_vrednost WHERE naziv = '".self::convert($characteristic_value)."'");
						if(isset($grupa_pr_vrednost[0])){
							DB::statement("INSERT INTO web_roba_karakteristike (roba_id,rbr,grupa_pr_naziv_id,vrednost,vrednost_css,grupa_pr_vrednost_id) VALUES (".$getMappedArticles[$article_characteristic->roba_id].",".(!is_null($article_characteristic->rbr) ? $article_characteristic->rbr : '0').",".$grupa_pr_vrednost[0]->grupa_pr_naziv_id.",'".self::convert($article_characteristic->vrednost)."','".$article_characteristic->vrednost_css."',".$grupa_pr_vrednost[0]->grupa_pr_vrednost_id.")");
						}
					}
				}
			}
		}



		$this->info('Data is geting successfully.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
